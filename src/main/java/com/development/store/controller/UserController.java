package com.development.store.controller;

import com.development.store.config.WebSecurittyConfig;
import com.development.store.dto.AddressDto;
import com.development.store.dto.CartDto;
import com.development.store.dto.UserDto;
import com.development.store.model.Address;
import com.development.store.model.Cart;
import com.development.store.model.User;
import com.development.store.service.AddressService;
import com.development.store.service.CartService;
import com.development.store.service.RequestService;
import com.development.store.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Collection;
import java.util.HashMap;

@ControllerAdvice
@Controller public class UserController {
// Public:
    @RequestMapping(value = "/user/api/signup", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<HashMap<String, String>> apiSignUp(@RequestBody UserDto dto) {
        HashMap<String, String> hashMap = new HashMap();

        //if (dto.getUsername().length() == 0 || dto.getPassword().length() == 0 || dto.getNewPassword().length() == 0 || dto.getEmail().length() == 0) {
        if (dto.getUsername().length() == 0 || dto.getPassword().length() == 0 || dto.getEmail().length() == 0) {
            hashMap.put("message", "Fill all the forms in order to create account");
            hashMap.put("color", "orange");
            return ResponseEntity.ok(hashMap);
        }

        // check for username duplication
        if (userService.duplicationUsername(dto.getUsername())) {
            hashMap.put("message", "Username is already in use, try another one");
            hashMap.put("color", "orange");
            return ResponseEntity.ok(hashMap);
        }

        // check for email duplication
        if (userService.duplicationEmail(dto.getUsername())) {
            hashMap.put("message", "Email is already in use, try another one");
            hashMap.put("color", "orange");
            return ResponseEntity.ok(hashMap);
        }

//        // check for password compability
//        if (!dto.getPassword().equals(dto.getNewPassword())) {
//            map.put("color", "chocolate");
//            map.put("message", "Password mismatch, check for compability");
//            return ResponseEntity.ok(map);
//        }

        // dto -> user

//        User user = new User(dto.getUsername(), bCrypt.encode(dto.getPassword()), dto.getEmail());

        //User user = new User();
        //user.setUsername(dto.getUsername());
        //user.setPassword(user.encryptPassword(dto.getPassword()));
        //user.setEmail(dto.getEmail());

        // saving account into database


//        // check for account in database
//        User existsUser = userRepository.findByUsername(user.getUsername());
//        System.err.println(user.getUsername());
//        if (existsUser != null) {
//            map.put("color", "red");
//            map.put("message", "Failed to save account into database, contact with support");
//            return ResponseEntity.ok(map);
//        }

        BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();
        dto.setPassword(bCrypt.encode(dto.getPassword()));

        userService.save(new User().getFromDto(dto));

        hashMap.put("message", "Account has been successfuly created! Check your email for activation link.");
        hashMap.put("color", "green");

        return ResponseEntity.ok(hashMap);
    }

    @RequestMapping(value = "/user/api/change-email", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<HashMap<String, String>> apiChangeEmail(@RequestBody UserDto dto) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getByUsername(authentication.getName());

        HashMap<String, String> hashMap = new HashMap();

        if (dto.getEmail().equalsIgnoreCase(user.getEmail())) {
            hashMap.put("message", "You need to fill the form with different email than actual!");
            hashMap.put("color", "orange");
            return ResponseEntity.ok(hashMap);
        }

        if (userService.getByEmail(dto.getEmail()) != null) {
            hashMap.put("message", "Email address is already in use, try another one.");
            hashMap.put("color", "orange");
            return ResponseEntity.ok(hashMap);
        }


        if (user != null) {
            user.setEmail(dto.getEmail());
            userService.save(user);
            hashMap.put("message", "Successfully changed email address!");
            hashMap.put("color", "green");
            return ResponseEntity.ok(hashMap);
        }
        else {
            hashMap.put("message", "Something went wrong, contact with support!");
            hashMap.put("color", "red");
            return ResponseEntity.ok(hashMap);
        }
    }

    @RequestMapping(value = "/user/api/change-address", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<HashMap<String, String>> apiChangeAddress(@RequestBody AddressDto dto) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getByUsername(authentication.getName());

        HashMap<String, String> hashMap = new HashMap();

        if (user != null) {
            Address address = userService.getAddress(user.getId());
            if (address == null) address = new Address();

            try {
                if (address.getName().equalsIgnoreCase(dto.getName()) &&
                        address.getLastName().equalsIgnoreCase(dto.getLastName()) &&
                        address.getAddress1().equalsIgnoreCase(dto.getAddress1()) &&
                        address.getAddress2().equalsIgnoreCase(dto.getAddress2()) &&
                        address.getCity().equalsIgnoreCase(dto.getCity()) &&
                        address.getPostalCode().equalsIgnoreCase(dto.getPostalCode()) &&
                        address.getCountry().equalsIgnoreCase(dto.getCountry()) &&
                        address.getPhone() == dto.getPhone()) {
                    hashMap.put("message", "No changes were made.");
                    hashMap.put("color", "orange");
                    return ResponseEntity.ok(hashMap);
                }
            }
            catch (Exception e) {}

            Address entity = address.getFromDto(dto);
            entity.setUserId(user.getId());

            addressService.save(entity);
            hashMap.put("message", "Successfully changed address!");
            hashMap.put("color", "green");
            return ResponseEntity.ok(hashMap);
        }
        else {
            hashMap.put("message", "Something went wrong, contact with support!");
            hashMap.put("color", "red");
            return ResponseEntity.ok(hashMap);
        }
    }

    @RequestMapping(value = "/user/api/change-password", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<HashMap<String, String>> apiChangePassword(@RequestBody UserDto dto) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getByUsername(authentication.getName());

        HashMap<String, String> hashMap = new HashMap();

        if (user != null) {
            if (!webSecurittyConfig.passwordEncoder().matches(dto.getPassword(), user.getPassword())) {
                hashMap.put("message", "Incorrect current password.");
                hashMap.put("color", "red");
                return ResponseEntity.ok(hashMap);
            }

            if (webSecurittyConfig.passwordEncoder().matches(dto.getNewPassword(), user.getPassword())) {
                hashMap.put("message", "Password must be different than actual!");
                hashMap.put("color", "orange");
                return ResponseEntity.ok(hashMap);
            }
        }

        if (user != null) {
            if (dto.getNewPassword().length() < User.MINIMUM_PASSWORD_LENGTH) {
                hashMap.put("message", "Password must contain at least " + User.MINIMUM_PASSWORD_LENGTH + " characters!");
                hashMap.put("color", "orange");
                return ResponseEntity.ok(hashMap);
            }

            user.setPassword(user.encryptPassword(dto.getNewPassword()));
            userService.save(user);

            hashMap.put("message", "Successfully changed password!");
            hashMap.put("color", "green");
            return ResponseEntity.ok(hashMap);
        }
        else {
            hashMap.put("message", "Something went wrong, contact with support!");
            hashMap.put("color", "red");
            return ResponseEntity.ok(hashMap);
        }

    }

    @GetMapping("/user/signup") public String signUp(Model model, HttpServletRequest request, Principal principal) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (requestService.redirect(authentication, request)) {
            return "user/signup";
        }

        return "redirect:/";
    }

    @PostMapping("/user/process_register") public String processRegistration(User user, HttpServletRequest request, Principal principal) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (!requestService.redirect(authentication, request, true))
            return "redirect:/";

        user.setPassword(webSecurittyConfig.passwordEncoder().encode(user.getPassword()));
        user.setRole("USER");
        user.setEnabled(false);
        user.setLocked(false);

        userService.save(user);
        return "redirect:/user/login?success";
    }

    @GetMapping("/user/profile") public String profilePage(Model model, HttpServletRequest request, Principal principal) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (requestService.redirect(authentication, request))
            return "user/login";
        else {
            String username = authentication.getName();
            User user = userService.getByUsername(username);
            model.addAttribute("profile", user);

            Address address = userService.getAddress(user.getId());
            if (address == null) address = new Address();
            model.addAttribute("address", address);

            return "user/profile";
        }
    }

    @GetMapping("/cart") public ModelAndView cart(Principal principal) {
        ModelAndView modelAndView = new ModelAndView("cart");

        Collection<Cart> cart = cartService.getCartItems();
        Collection<CartDto> cartDto = cartService.entityToDto(cart);

        modelAndView.addObject("cartDto", cartDto);
        modelAndView.addObject("total", cartService.getTotal(cart));
        modelAndView.addObject("entry", cartDto.size() > 9 ? "9+" : cartDto.size());

        return modelAndView;
    }

// Private:
    @Autowired private RequestService requestService;
    @Autowired private WebSecurittyConfig webSecurittyConfig;
    @Autowired private CartService cartService;
    @Autowired private UserService userService;
    @Autowired private AddressService addressService;
}
