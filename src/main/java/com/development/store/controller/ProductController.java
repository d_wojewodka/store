package com.development.store.controller;

import com.development.store.dto.ProductDto;
import com.development.store.model.Product;
import com.development.store.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;

@Controller
public class ProductController {
// Public:
    @RequestMapping(value = "/product/category{name}")
    public String getCategory(@RequestParam("name") String filter, Model model, Principal principal) {
        Collection<Product> items = productService.getByCategory(filter);
        Collection<ProductDto> productDto = new ArrayList();

        for (Product product : items) {
            productDto.add(new ProductDto().getFromEntity(product));
        }

        model.addAttribute("category", filter);
        model.addAttribute("productDto", productDto);

        return "product/category";
    }

    @RequestMapping(value = "/product/search{query}")
    public String search(@RequestParam("text") String text, Model model, Principal principal) {
        Collection<Product> items = productService.search(text);
        Collection<ProductDto> productDto = new ArrayList();

        for (Product product : items) {
            productDto.add(new ProductDto().getFromEntity(product));
        }

        model.addAttribute("productDto", productDto);
        model.addAttribute("entries", productDto.size());
        model.addAttribute("text", text);

        return "product/search";
    }

    @GetMapping("/product/view/{productId}") public String viewProduct(@PathVariable("productId") long productId, Model model, Principal principal) {
        Product product = productService.getById(productId);

        model.addAttribute("product", product);
        return "product/view";
    }


//    @GetMapping("/product/modify/{productId}") public String editProduct(@PathVariable("productId") long productId, Model model, Principal principal) {
//        Product product = productRepository.getById(productId);
//
//        model.addAttribute("product", product);
//        return "product/modify";
//    }
//
//    @GetMapping("/product/new") public String newProduct(Model model, Principal principal) {
//        model.addAttribute("product", new Product());
//
//        return "product/new";
//    }

// Private:
    @Autowired private ProductService productService;
}
