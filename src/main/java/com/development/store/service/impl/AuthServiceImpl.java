package com.development.store.service.impl;

import com.development.store.service.AuthService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {
// Public:
    @Override
    public boolean isLoggedIn() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication.getPrincipal() instanceof UserDetails)
            return true;

        return false;
    }

    @Override
    public String getName() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object object = authentication.getPrincipal();

        if (object instanceof UserDetails)
            return ((UserDetails) object).getUsername();

        return null;
    }
// Private:
}
