package com.development.store.model;

import com.development.store.constants.GLOBAL;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "discount", schema = GLOBAL.SCHEMA_DATA, catalog = GLOBAL.SCHEMA_DATA)
public class Discount {
// Public:
    public Discount() {};

// Private:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) private long id;
    @Column(name="code", nullable = false, unique = true) private String code;
    @Column(name="value", nullable = false, unique = true) private String value;
    @Column(name="percent", nullable = false, unique = true) private String percent;
    @Column(name="products", nullable = false, unique = true) private String products;
    @Column(name="categories", nullable = false, unique = true) private String categories;
    @Column(name="limit", nullable = false, unique = true) private String limit;
    @Column(name="expiry", nullable = false, unique = true) private Timestamp expiry;
//  +----+------+-------+---------+----------+------------+-------+--------+
//  | id | code | value | percent | products | categories | limit | expiry |
//  +====+======+=======+=========+==========+============+=======+========+

}
