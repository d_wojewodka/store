package com.development.store.service.impl;

import com.development.store.model.User;
import com.development.store.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserDetailsServiceImpl implements UserDetailsService {
// Public:
    @Override public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        User user = userRepository.getByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("Cannot find the user: " + username);
        }

        return new UserDetailsImpl(user);
    }

// Private:
    @Autowired private UserRepository userRepository;
}
