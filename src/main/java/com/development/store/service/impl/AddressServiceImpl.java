package com.development.store.service.impl;

import com.development.store.model.Address;
import com.development.store.repository.AddressRepository;
import com.development.store.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Objects;

@Service
public class AddressServiceImpl implements AddressService {
// Public:
    public AddressServiceImpl() {}
    public AddressServiceImpl(final AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public Address save(final Address address){
        try {
            Address newAddress = addressRepository.save(address);
            return newAddress;
        }
        catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean delete(final Address address) {
        try {
            addressRepository.delete(address);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public Collection<Address> getAll() {
        Collection<Address> addresses = addressRepository.findAll();
        return Objects.nonNull(addresses) ? addresses : null;
    }

    @Override
    public Address getById(final long id) {
        Address address = addressRepository.getById(id);
        return Objects.nonNull(address) ? address : null;
    }

    @Override
    public Address getByUserId(final long id) {
        Address address = addressRepository.getByUserId(id);
        return Objects.nonNull(address) ? address : null;
    }

    // Private:
    @Autowired private AddressRepository addressRepository;
}
