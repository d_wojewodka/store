package com.development.store.controller;

import com.development.store.dto.CartDto;
import com.development.store.dto.OrderDto;
import com.development.store.model.Address;
import com.development.store.model.Cart;
import com.development.store.model.Delivery;
import com.development.store.model.User;
import com.development.store.service.CartService;
import com.development.store.service.DeliveryService;
import com.development.store.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Collection;

@Controller
public class OrderController {
// Public:
    @GetMapping("/order/finalize") public ModelAndView finalizeOrder(Principal principal) {
        ModelAndView modelAndView = new ModelAndView("order/finalize");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication.getName() == "anonymousUser")
            return new ModelAndView("user/login");

        Collection<Cart> cart = cartService.getCartItems();
        Collection<CartDto> cartDto = cartService.entityToDto(cart);
        Collection<Delivery> delivery = deliveryService.getAvailable();

        if (cart.size() == 0)
            return new ModelAndView("user/cart");

        modelAndView.addObject("cartDto", cartDto);
        modelAndView.addObject("total", cartService.getTotal(cart));
        modelAndView.addObject("entry", cartDto.size() > 9 ? "9+" : cartDto.size());
        modelAndView.addObject("deliveryDto", delivery);

        String username = authentication.getName();
        User user = userSerivce.getByUsername(username);
        Address address = userSerivce.getAddress(user.getId());
        if (address == null) address = new Address();

        modelAndView.addObject("address", address);

        return modelAndView;
    }

    @GetMapping("/order/payment") public String payment(Model model, HttpServletRequest request, Principal principal, @RequestBody final OrderDto dto) {
        Collection<Cart> cart = cartService.getCartItems();
        Collection<CartDto> cartDto = cartService.entityToDto(cart);

        model.addAttribute("cartDto", cartDto);
        model.addAttribute("total", cartService.getTotal(cart));
        model.addAttribute("entry", cartDto.size() > 9 ? "9+" : cartDto.size());

        return "order/payment";
    }

// Private:
    @Autowired private CartService cartService;
    @Autowired private UserService userSerivce;
    @Autowired private DeliveryService deliveryService;
}
