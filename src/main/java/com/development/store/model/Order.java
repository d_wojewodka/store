package com.development.store.model;

import com.development.store.constants.GLOBAL;
import com.development.store.dto.OrderDto;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Table(name = "order", schema = GLOBAL.SCHEMA_DATA, catalog = GLOBAL.SCHEMA_DATA)
public class Order {
// Public:
    public Order() {}

    public void getFromDto(OrderDto dto) {
        return;
    }

    public long getId() { return id; }
    public long getUserId() { return userId; }
    public long getCartId() { return cartId; }
    public int getDeliveryType() { return deliveryType; }
    public String getDeliveryCredentials() { return deliveryCredentials; }
    public String getDeliveryAddress1() { return deliveryAddress1; }
    public String getDeliveryAddress2() { return deliveryAddress2; }
    public Long getDeliveryPhone() { return deliveryPhone; }
    public String getDeliveryNote() { return deliveryNote; }
    public BigDecimal getDeliveryWeight() { return deliveryWeight; }
    public BigDecimal getDeliveryPrice() { return deliveryPrice; }
    public Timestamp getDeliveryETA() { return deliveryETA; }
    public BigDecimal getTotalPrice() { return totalPrice; }
    public String getDiscount() { return discount; }
    public BigDecimal getFinalPrice() { return finalPrice; }
    public int getPayment() { return payment; }
    public int getStatus() { return status; }
    public Timestamp getCreateAt() { return createAt; }
    public Timestamp getUpdateAt() { return updateAt; }

    public void setId(final long id) { this.id = id; }
    public void setUserId(final long userId) { this.userId = userId; }
    public void setCartId(final long cartId) { this.cartId = cartId; }
    public void setDeliveryType(final int deliveryType) { this.deliveryType = deliveryType; }
    public void setDeliveryCredentials(final String deliveryCredentials) { this.deliveryCredentials = deliveryCredentials; }
    public void setDeliveryAddress1(final String deliveryAddress1) { this.deliveryAddress1 = deliveryAddress1; }
    public void setDeliveryAddress2(final String deliveryAddress2) { this.deliveryAddress2 = deliveryAddress2; }
    public void setDeliveryPhone(final Long deliveryPhone) { this.deliveryPhone = deliveryPhone; }
    public void setDeliveryNote(final String deliveryNote) { this.deliveryNote = deliveryNote; }
    public void setDeliveryWeight(final BigDecimal deliveryWeight) { this.deliveryWeight = deliveryWeight; }
    public void setDeliveryPrice(final BigDecimal deliveryPrice) { this.deliveryPrice = deliveryPrice; }
    public void setDeliveryETA(final Timestamp deliveryETA) { this.deliveryETA = deliveryETA; }
    public void setTotalPrice(final BigDecimal totalPrice) { this.totalPrice = totalPrice; }
    public void setDiscount(final String discount) { this.discount = discount; }
    public void setFinalPrice(final BigDecimal finalPrice) { this.finalPrice = finalPrice; }
    public void setPayment(final int payment) { this.payment = payment; }
    public void setStatus(final int status) { this.status = status; }
    public void setCreateAt(final Timestamp createAt) { this.createAt = createAt; }
    public void setUpdateAt(final Timestamp updateAt) { this.updateAt = updateAt; }

    // Private:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) private long id;
    @Column(name="user_id", nullable = false) private long userId;
    @Column(name="cart_id", nullable = false) private long cartId;
    @Column(name="delivery_type", nullable = false) private int deliveryType;
    @Column(name="delivery_credentials", nullable = false) private String deliveryCredentials;
    @Column(name="delivery_address1", nullable = false) private String deliveryAddress1;
    @Column(name="delivery_address2", nullable = false) private String deliveryAddress2;
    @Column(name="delivery_phone", nullable = false) private Long deliveryPhone;
    @Column(name="delivery_note", nullable = false) private String deliveryNote;
    @Column(name="delivery_weight", nullable = false) private BigDecimal deliveryWeight;
    @Column(name="delivery_price", nullable = false) private BigDecimal deliveryPrice;
    @Column(name="delivery_eta", nullable = false) private Timestamp deliveryETA;
    @Column(name="total_price", nullable = false) private BigDecimal totalPrice;
    @Column(name="discount", nullable = false) private String discount;
    @Column(name="final_price", nullable = false) private BigDecimal finalPrice;
    @Column(name="payment", nullable = false) private int payment;
    @Column(name="status", nullable = false) private int status;

    @CreationTimestamp
    @Column(name="create_at", nullable = false, updatable = false) private Timestamp createAt;

    @UpdateTimestamp
    @Column(name="update_at") private Timestamp updateAt;
}
