package com.development.store.repository;

import com.development.store.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface CartRepository extends JpaRepository<Cart, Long> {
// Public:
    @Query("SELECT r FROM Cart r WHERE r.userId = :userId")
    Collection<Cart> getByUser(@Param("userId") long userId);
    @Query("SELECT r FROM Cart r WHERE r.productId = :productId AND r.userId = :userId")
    Cart getByProduct(@Param("productId") long productId, @Param("userId") long userId);
}