package com.development.store.service;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

public interface RequestService {
    String getClientIp(HttpServletRequest request);
    boolean redirect(Authentication authentication, HttpServletRequest request);
    boolean redirect(Authentication authentication, HttpServletRequest request, boolean appendLog);
}
