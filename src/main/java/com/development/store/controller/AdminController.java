package com.development.store.controller;

import com.development.store.model.Address;
import com.development.store.model.Product;
import com.development.store.model.User;
import com.development.store.service.AddressService;
import com.development.store.service.PageService;
import com.development.store.service.ProductService;
import com.development.store.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


@Controller public class AdminController {
// Public:
    @GetMapping("/admin/accounts") public String accounts(Model model, Principal principal) {
        Collection<User> users = userService.getAll();
        model.addAttribute("accountList", users);

        return pageService.redirect("/admin/accounts");
    }

    @PostMapping("/admin/change_role") public String changeRole(@RequestParam("username") String username, @RequestParam("role") String role, HttpServletRequest request, Principal principal, HttpSession session) {
        User user = userService.getByUsername(username);

        if (user.getRole().equalsIgnoreCase(role))
            return "admin/accounts";

        user.setRole(role);
        userService.save(user);

        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority(user.getRole()));
        Authentication refreshAuth = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), authorities);
        SecurityContextHolder.getContext().setAuthentication(refreshAuth);


        // TODO update roles in securityContextHolder

        if (user.getUsername().equalsIgnoreCase(principal.getName()))
            return "/index";
        else
            return "redirect:/admin/accounts";
    }

    @GetMapping("/admin/products") public String products(Model model, Principal principal) {
        Collection<Product> products = productService.getAll();
        model.addAttribute("productList", products);

        return pageService.redirect("/admin/products");
    }

    @GetMapping("/admin/addresses") public String addresses(Model model, Principal principal) {
        Collection<Address> addresses = addressService.getAll();
        model.addAttribute("addressList", addresses);

        return pageService.redirect("/admin/addresses");
    }

// Private:
    @Autowired private UserService userService;
    @Autowired private AddressService addressService;
    @Autowired private ProductService productService;
    @Autowired private PageService pageService;
}
