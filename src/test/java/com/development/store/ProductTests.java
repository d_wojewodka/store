package com.development.store;

import com.development.store.model.Product;
import com.development.store.repository.ProductRepository;
import com.development.store.service.ProductService;
import com.development.store.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

@ExtendWith(MockitoExtension.class)
public class ProductTests {
// Public:
    @Test @Order(1)
    public void save() {
        // given
        Product product = new Product();
        product.setId(1);

        // then
        Mockito.lenient().when(productRepository.save(Mockito.any(Product.class))).thenReturn(product);
        Product newProduct = productService.save(product);

        Assertions.assertNotNull(newProduct);
        Assertions.assertEquals(1, newProduct.getId());

        // given
        product.setName("NAME");
        product.setCategory("CATEGORY");
        product.setCollection("COLLECTION");
        product.setDescription("DESCRIPTION");
        product.setManufacturer("MANUFACTURER");
        product.setPhoto("PHOTO");
        product.setPhotoList("PHOTOLIST");
        product.setPrice(BigDecimal.ZERO);
        product.setQuantity(1);

        // then
        newProduct = productService.save(product);

        Assertions.assertNotNull(newProduct);
        Assertions.assertEquals(1, newProduct.getId());
        Assertions.assertEquals("NAME", newProduct.getName());
        Assertions.assertEquals("CATEGORY", newProduct.getCategory());
        Assertions.assertEquals("COLLECTION", newProduct.getCollection());
        Assertions.assertEquals("DESCRIPTION", newProduct.getDescription());
        Assertions.assertEquals("MANUFACTURER", newProduct.getManufacturer());
        Assertions.assertEquals("PHOTO", newProduct.getPhoto());
        Assertions.assertEquals("PHOTOLIST", newProduct.getPhotoList());
        Assertions.assertEquals(BigDecimal.ZERO, newProduct.getPrice());
        Assertions.assertEquals(1, newProduct.getQuantity());
    }

    @Test @Order(2)
    public void get() {
        // given
        Product product = new Product();
        product.setId(1);

        Collection<Product> products = new ArrayList();
        for (int i = 1; i < 3; i++) {
            Product entity = new Product();
            entity.setId(i);
            entity.setName("NAME");
            entity.setCategory("CATEGORY");
            entity.setCollection("COLLECTION");
            products.add(entity);
        }

        // then
        Mockito.lenient().when(productRepository.save(Mockito.any(Product.class))).thenReturn(product);
        Mockito.lenient().when(productRepository.getById(Mockito.any(long.class))).thenReturn(product);
        Mockito.lenient().when(productRepository.getByName(Mockito.any(String.class))).thenReturn(products);
        Mockito.lenient().when(productRepository.getByCategory(Mockito.any(String.class))).thenReturn(products);
        Mockito.lenient().when(productRepository.getByPhrase(Mockito.any(String.class))).thenReturn(products);

        Product newProduct = productService.save(product);

        Assertions.assertNotNull(product);
        Assertions.assertNotNull(productService.getById(newProduct.getId()));
        Assertions.assertNotNull(productService.getByName(newProduct.getName()));
        Assertions.assertNotNull(productService.getByCategory(newProduct.getCategory()));
        Assertions.assertNotNull(productService.search("CAT"));
        Assertions.assertNotEquals(null, productService.getByName("NAME"));
        Assertions.assertNotEquals(null, productService.getByCategory("CATEGORY"));
        Assertions.assertNotEquals(null, productService.search("NA"));
    }

// Private:
    @InjectMocks
    private ProductService productService = new ProductServiceImpl();

    @Mock
    private ProductRepository productRepository;
}