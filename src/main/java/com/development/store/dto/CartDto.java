package com.development.store.dto;

import com.development.store.model.Cart;

import java.sql.Timestamp;
import java.util.Objects;

public class CartDto {
// Public:
    public CartDto() {}
    public CartDto(final long productId, final int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }
    public CartDto(final ProductDto productDto, final int quantity) {
        this.productDto = productDto;
        this.quantity = quantity;
    }


    public long getProductId() {
        return productId;
    }
    public ProductDto getProductDto() {
        return productDto;
    }
    public int getQuantity() {
        return quantity;
    }
    public Timestamp getCreateAt() { return createAt; }
    public Timestamp getUpdateAt() { return updateAt; }

    public void setProductId(final long productId) {
        this.productId = productId;
    }
    public void setProductDto(final ProductDto productDto) {
        this.productDto = productDto;
    }
    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }
    public void setCreateAt(final Timestamp createAt) { this.createAt = createAt; }
    public void setUpdateAt(final Timestamp updateAt) { this.updateAt = updateAt; }

    public CartDto getFromEntity(Cart cart) {
        productId = Objects.nonNull(cart.getProductId()) ? cart.getProductId() : null;
        quantity = Objects.nonNull(cart.getQuantity()) ? cart.getQuantity() : null;
        createAt = Objects.nonNull(cart.getCreateAt()) ? cart.getCreateAt() : null;
        updateAt = Objects.nonNull(cart.getUpdateAt()) ? cart.getUpdateAt() : null;

        return this;
    }

// Private:
    private long productId;
    private int quantity;
    private Timestamp createAt;
    private Timestamp updateAt;

    private ProductDto productDto;



//    private ProductDto productDto;
//    public void setProductDto(ProductDto productDto) { this.productDto = productDto; }
//    public ProductDto getProductDto() { return productDto; }
//    public CartDto(ProductDto productDto, int quantity) {
//        this.productDto = productDto;
//        this.quantity = quantity;
//    }
}
