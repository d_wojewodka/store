package com.development.store.service.impl;

import com.development.store.dto.ProductDto;
import com.development.store.model.Product;
import com.development.store.repository.ProductRepository;
import com.development.store.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Service
public class ProductServiceImpl implements ProductService {
// Public:
    public ProductServiceImpl() {}
    public ProductServiceImpl(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product save(final Product product) {
        try {
            Product newProduct = productRepository.save(product);
            return newProduct;
        }
        catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean delete(final Product product) {
        try {
            productRepository.delete(product);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public String checkProductQuantity(final long productId, final int quantity) {
        String message = "";
        Product product = productRepository.getById(productId);

        try {
            if (product.getQuantity() == 0)
                message = "Product is currently unavailable";
            else if (product.getQuantity() < (0 + quantity))
                message = "We dont have enough quantity of this product";
        } catch (Exception e) {
            message = "There is no such a product";
        }

        return message;
    }

    @Override
    public Collection<ProductDto> entityToDto(final Collection<Product> entity) {
        Collection <ProductDto> dto = new ArrayList();

        for (Product product : entity) {
            dto.add(new ProductDto().getFromEntity(product));
        }

        return dto;
    }

    @Override
    public Collection<Product> getAll() {
        Collection<Product> products = productRepository.findAll();
        return Objects.nonNull(products) ? products : null;
    }

    @Override
    public Product getById(final long productId) {
        Product product = productRepository.getById(productId);
        return Objects.nonNull(product) ? product : null;
    }

    @Override
    public Collection<Product> getByName(final String name) {
        Collection<Product> products = productRepository.getByName("NAME");
        return Objects.nonNull(products) ? products : null;
    }

    @Override
    public Collection<Product> getByCategory(final String filter) {
        Collection<Product> products = productRepository.getByCategory(filter);
        return Objects.nonNull(products) ? products : null;
    }

    @Override
    public Collection<Product> search(final String text) {
        Collection<Product> products = productRepository.getByPhrase(text);
        return Objects.nonNull(products) ? products : null;
    }

// Private:
    @Autowired private ProductRepository productRepository;
}
