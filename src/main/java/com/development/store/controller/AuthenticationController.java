package com.development.store.controller;

import com.development.store.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;


@Controller public class AuthenticationController {
// Public:
    @GetMapping("/user/login") public String loginPage(HttpServletRequest request, Principal principal) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (requestService.redirect(authentication, request))
            return "user/login";

        return "redirect:/";
    }

// Private:
    @Autowired private RequestService requestService;
}



//@Component
//public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {
//    @Override
//    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
//        System.out.println(exception.getMessage());
//
//        request.setAttribute("param", "error");
//        response.sendRedirect("/user/login?error");
//
//        String email = request.getParameter("email");
//        String error = exception.getMessage();
//        System.out.println("A failed login attempt with email: "
//                + email + ". Reason: " + error);
//
//        super.setDefaultFailureUrl("/login?error");
//        super.onAuthenticationFailure(request, response, exception);
//    }
//}

//@Component public class CustomLoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
//    // Public:
//    @Override public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
//        // Code For Business Here
//        Log.putThread(LogType.LOG, "onAuthenticationSuccess", requestService.getClientIp(request), "User2 '" + authentication.getName() + "' has logged in");
//
//        //response.setStatus(HttpServletResponse.SC_OK);
//        //response.sendRedirect("redirect:/index");
//        super.onAuthenticationSuccess(request, response, authentication);
//    }
//
//    // Private:
//    @Autowired private RequestService requestService;
//}

//@Component public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {
//    @Override public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
//        // Code For Business Here
//        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
//        Log.putThread(LogType.LOG, "onLogoutSuccess", requestService.getClientIp(request), "User2 '" + authentication.getName() + "' has logged out");
//
//        response.setStatus(HttpServletResponse.SC_OK);
//        response.sendRedirect("/user/login?logout");
//    }
//
//
//    // Private:
//    @Autowired private RequestService requestService;
//}
