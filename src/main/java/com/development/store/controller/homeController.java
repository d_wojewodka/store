package com.development.store.controller;

import com.development.store.constants.GLOBAL;
import com.development.store.dto.CartDto;
import com.development.store.dto.ProductDto;
import com.development.store.model.Cart;
import com.development.store.model.Product;
import com.development.store.service.CartService;
import com.development.store.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;
import java.util.Collection;

@Controller
@ControllerAdvice
public class homeController {
// Public:
    @GetMapping("uiux") public String UIUX(HttpSession session) {
        return "uiux";
    }

    @GetMapping("") public String defaultPage(HttpSession session) {
        return "index";
    }

    @GetMapping("home") public String homePage(HttpSession session) {
        return "home";
    }

    @ModelAttribute public void getCart(Model model) {
        Collection<Cart> cart = cartService.getCartItems();
        Collection<CartDto> cartDto = cartService.entityToDto(cart);

        model.addAttribute("cartDto", cartDto);
        model.addAttribute("total", cartService.getTotal(cart));
        model.addAttribute("entry", cartDto.size() > 9 ? "9+" : cartDto.size());
    }

    @ModelAttribute public void getCategory(Model model) {
        Collection<Product> items = productService.getAll();
        Collection<ProductDto> productDto = productService.entityToDto(items);

        model.addAttribute("category", "new");
        model.addAttribute("productDto", productDto);
    }

    @ModelAttribute public void getNotify(Model model) {
        model.addAttribute("notify", "Current version " + GLOBAL.VERSION + ", updated at " + GLOBAL.LAST_UPDATE);
    }

// Private:
    @Autowired private ProductService productService;
    @Autowired private CartService cartService;
}
