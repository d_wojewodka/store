package com.development.store;

import com.development.store.model.Address;
import com.development.store.repository.AddressRepository;
import com.development.store.service.AddressService;
import com.development.store.service.impl.AddressServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AddressTests {
// Public:
    @Test @Order(1)
    public void save() {
        // given
        Address address = new Address();
        address.setId(1);

        // then
        Mockito.lenient().when(addressRepository.save(Mockito.any(Address.class))).thenReturn(address);
        Address newAddress = addressService.save(address);

        Assertions.assertNotNull(newAddress);
        Assertions.assertEquals(1, newAddress.getId());

        // given
        address.setUserId(1);
        address.setName("NAME");
        address.setLastName("LASTNAME");
        address.setAddress1("ADDRESS1");
        address.setAddress2("ADDRESS2");
        address.setCity("CITY");
        address.setPostalCode("POSTALCODE");
        address.setCountry("COUNTRY");
        address.setPhone(1L);

        // then
        newAddress = addressService.save(address);

        Assertions.assertNotNull(newAddress);
        Assertions.assertEquals(1, newAddress.getId());
        Assertions.assertEquals("NAME", newAddress.getName());
        Assertions.assertEquals("LASTNAME", newAddress.getLastName());
        Assertions.assertEquals("ADDRESS1", newAddress.getAddress1());
        Assertions.assertEquals("ADDRESS2", newAddress.getAddress2());
        Assertions.assertEquals("CITY", newAddress.getCity());
        Assertions.assertEquals("POSTALCODE", newAddress.getPostalCode());
        Assertions.assertEquals("COUNTRY", newAddress.getCountry());
        Assertions.assertEquals(1L, newAddress.getPhone());
    }

    @Test @Order(2)
    public void get() {
        // given
        Address address = new Address();
        address.setId(1);
        address.setName("NAME");
        address.setLastName("LASTNAME");

        // then
        Mockito.lenient().when(addressRepository.save(Mockito.any(Address.class))).thenReturn(address);
        Mockito.lenient().when(addressRepository.getById(Mockito.any(long.class))).thenReturn(address);
        Mockito.lenient().when(addressRepository.getByUserId(Mockito.any(long.class))).thenReturn(address);

        Address newAddress = addressService.save(address);

        Assertions.assertNotNull(address);
        Assertions.assertNotNull(addressService.getById(newAddress.getId()));
        Assertions.assertNotNull(addressService.getByUserId(newAddress.getUserId()));
    }

// Private:
    @InjectMocks
    private AddressService addressService = new AddressServiceImpl();

    @Mock
    private AddressRepository addressRepository;

}