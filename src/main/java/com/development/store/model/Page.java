package com.development.store.model;

import com.development.store.constants.GLOBAL;

import javax.persistence.*;

@Entity
@Table(name = "page", schema = GLOBAL.SCHEMA_ACCOUNT, catalog = GLOBAL.SCHEMA_ACCOUNT)
public class Page {
    // Public:
    public Page() {}
    public Page(final String page, final int requiredPower) {
        this.page = page;
        this.requiredPower = requiredPower;
    }

    public long getId() { return id; }
    public String getPage() { return page; }
    public int getRequiredPower() { return requiredPower; }

    public void setId(final long id) { this.id = id; }
    public void setPage(final String page) { this.page = page; }
    public void setRequiredPower(final int requiredPower) { this.requiredPower = requiredPower; }

    @Override
    public String toString() {
        return "Role:" + "\n" +
                "\tid=" + id + "\n" +
                "\tpage=" + page + "\n" +
                "\trequired_power=" + requiredPower + "\n";
    }

    // Private:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) private long id;
    @Column(name = "page", nullable = false, unique = true) private String page;
    @Column(name = "required_power", nullable = false) private int requiredPower;
}
