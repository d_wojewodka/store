package com.development.store.service;

import com.development.store.model.Address;
import com.development.store.model.Cart;
import com.development.store.model.User;

import java.util.Collection;

public interface UserService {
    User save(User user);
    boolean delete(User user);

    Collection<User> getAll();
    User getByUsername(String username);
    User getByEmail(String email);
    User getById(long userId);

    boolean duplicationUsername(String username);
    boolean duplicationEmail(String email);

    String encryptPassword(String raw_password);
    boolean validatePassword(String raw_password, String encrypted_password);

    User changePassword(User user, String current_password, String new_password);

    Address getAddress(long userId);
    Collection<Cart> getCart(long userId);
    //      Order getOrder(long userId);

    int getRolePower();
    long getId();
}
