package com.development.store.service;

import com.development.store.model.Delivery;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;

public interface DeliveryService {
    Delivery save(Delivery delivery);
    boolean delete(Delivery delivery);

    BigDecimal calculateDeliveryCost(int deliveryId);
    Collection<Delivery> getAvailable();
    HashMap<String, BigDecimal> getAvailableDeliveries();
}
