package com.development.store.service;

import com.development.store.model.Address;
import com.development.store.model.Order;

public interface OrderService {
    Order save(Order order);
    boolean delete(Order order);
    boolean initializeOrder(long userId, Address address);
}
