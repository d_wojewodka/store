package com.development.store.repository;

import com.development.store.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AddressRepository extends JpaRepository<Address, Long> {
    @Query("SELECT r FROM Address r WHERE r.userId = :userId")
        Address getByUserId(@Param("userId") long userId);
}
