# dwstore.herokuapp.com - for live preview


> > Todo list:

| Status | type | Description |
| ------ | ------ | ------ |
| `PENDING` | `SECURITY_ISSUE` `EXPLOIT` | refactor project for possible memory leaks, security issues, exploits and bugs |
| `PENDING` | `SECURITY_ISSUE` `EXPLOIT` | user data from Entity into DTO |
| `PENDING` | `FEATURE` | finalizing order |
| `PENDING` | `FEATURE` | payment |
| `PENDING` | `FEATURE` | deliver |

###

> Implemented:

| Status | type | Description |
| ------ | ------ | ------ |
| `COMPLETED` | `DESIGN` `NULL_ARGUMENT` | disable search when there is no text in input and add "X" button to clear text if length > 0 |
| `COMPLETED` | `PERFORMANCE` | improved cart performance by avoiding new object creation |
| `COMPLETED` | `PERFORMANCE` | ...Maps to HashMap |
| `COMPLETED` | `SECURITY_ISSUE` `EXPLOIT` | validate product quantity when adding or removing product |



/cart
/transaction/order
/transaction/payment
