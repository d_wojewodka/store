package com.development.store.dto;

import com.development.store.model.User;

import java.util.Objects;

public class UserDto {
// Public:
    UserDto() {}

    public long getUserId() { return userId; }
    public String getUsername() { return username; }
    public String getPassword() { return password; }
    public String getNewPassword() { return newPassword; }
    public String getEmail() { return email; }

    public void setId(final long userId) { this.userId = userId; }
    public void setUsername(final String username) { this.username = username; }
    public void setPassword(final String password) { this.password = password; }
    public void setNewPassword(final String newPassword) { this.newPassword = newPassword; }
    public void setEmail(final String email) { this.email = email; }

    public UserDto getFromEntity(User user) {
        userId = Objects.nonNull(user.getId()) ? user.getId() : null;
        username = Objects.nonNull(user.getUsername()) ? user.getUsername() : null;
        password = Objects.nonNull(user.getPassword()) ? user.getPassword() : null;
        email = Objects.nonNull(user.getEmail()) ? user.getEmail() : null;

        return this;
    }

// Private:
    private long userId;
    private String username;
    private String password;
    private String newPassword;
    private String email;
}
