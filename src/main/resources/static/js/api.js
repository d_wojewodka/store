function signUp() {
    if ($("#username").val().length == 0) $("#username").addClass("error");
    else $("#username").removeClass("error");

    if ($("#password").val().length == 0) $("#password").addClass("error");
    else $("#password").removeClass("error");

    if ($("#passwordConfirm").val().length == 0) $("#passwordConfirm").addClass("error");
    else $("#passwordConfirm").removeClass("error");

    if ($("#email").val().length == 0) $("#email").addClass("error");
    else $("#email").removeClass("error");

    if ($("#password").val() != $("#passwordConfirm").val()) {
        $("#passwordConfirm").addClass("error");
        $("#password").addClass("error");
        $("#popup-title").css("color", "chocolate").text("Failed");
        $("#popup-message").text("Password mismatch, correct those fields!");
        $("#popup-background").css("visibility", "visible").fadeIn(500);
        return;
    }

    if (!$("#TOS").is(":checked")) {
        $("#TOS").addClass("error");
        $("#popup-title").css("color", "chocolate").text("Failed");
        $("#popup-message").text("Terms Of Service needs to be accepted in order to create an account!");
        $("#popup-background").css("visibility", "visible").fadeIn(500);
        return;
    }
    else $("#TOS").removeClass("error");

    // abort if any field founds an error
    if ($("#signup-form").find(".error").length != 0) {
        $("#popup-title").css("color", "chocolate").text("Failed");
        $("#popup-message").text("Fields does not meet the requirement, correct them!");
        $("#popup-background").css("visibility", "visible").fadeIn(500);
        return;
    }

    let data = {
        username: $("#username").val(),
        password: $("#password").val(),
        email: $("#email").val()
    };

    $.ajax({
        type: 'POST',
        url: 'api/signup',
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            let parse = new Map(Object.entries(response));
            if (parse.get("color") == "green") {
                $("#popup-title").css("color", "green").text("Success");
            }
            else $("#popup-title").css("color", "chocolate").text("Failed");

            $("#popup-message").text(parse.get("message"));
            $("#popup-background").css("visibility", "visible").fadeIn(500);

        },
        error: function () {
            $("#popup-title").text("Error").css("color", "red");
            $("#popup-message").text("Error occurred, contact with support");
            $("#popup-background").css("visibility", "visible").fadeIn(500);
        }
    })
}

function changeEmail() {
    let data = {
        email: $("#email").val(),
    };

    $.ajax({
        type: 'POST',
        url: 'api/change-email',
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            let parse = new Map(Object.entries(response));
            $("#popup-title").text("Response");
            $("#popup-message").text(parse.get("message"));
            if (parse.get("color") != null)
                $("#popup-message").css("color", parse.get("color"))
            $("#popup-background").css("visibility", "visible").fadeIn(500);
        },
        error: function () {
            $("#popup-title").text("Error").css("color", "red");
            $("#popup-message").text("Error occurred, contact with support");
            $("#popup-background").css("visibility", "visible").fadeIn(500);
        }
    })
}
function changePassword() {
    let data = {
        password: $("#password").val(),
        newPassword: $("#new-password").val(),
    };

    $.ajax({
        type: 'POST',
        url: 'api/change-password',
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            let parse = new Map(Object.entries(response));
            $("#popup-title").text("Response");
            $("#popup-message").text(parse.get("message"));
            if (parse.get("color") != null)
                $("#popup-message").css("color", parse.get("color"))
            $("#popup-background").css("visibility", "visible").fadeIn(500);
        },
        error: function () {
            $("#popup-title").text("Error").css("color", "red");
            $("#popup-message").text("Error occurred, contact with support");
            $("#popup-background").css("visibility", "visible").fadeIn(500);
        }
    })
}

function changeAddress() {
    let data = {
            name: $("#name").val(),
            lastName: $("#last_name").val(),
            address1: $("#address1").val(),
            address2: $("#address2").val(),
            city: $("#city").val(),
            postalCode: $("#postal_code").val(),
            country: $("#country").val(),
            phone: $("#phone").val()
    };

    $.ajax({
        type: 'POST',
        url: 'api/change-address',
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            let parse = new Map(Object.entries(response));
            $("#popup-title").text("Response");
            if (parse.get("color") != null)
                $("#popup-message").css("color", parse.get("color"))
            $("#popup-message").text(parse.get("message"));
            $("#popup-background").css("visibility", "visible").fadeIn(500);
        },
        error: function () {
            $("#popup-title").text("Error").css("color", "red");
            $("#popup-message").text("Error occurred, contact with support");
            $("#popup-background").css("visibility", "visible").fadeIn(500);
        }
    })
}

function addProductToCart(productId) {
    let data = {
            productId: productId,
            quantity: 1
            //quantity: $("#" + productId).find("quantity").val()
    };

    $.ajax({
        type: 'POST',
        url: '../api/addProduct',
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            let parse = new Map(Object.entries(response));

            if (parse.get("error_message") != null) {
                $("#popup-title").text("Warning").css("color", "#ffae00");
                $("#popup-message").text(parse.get("error_message"));
                $("#popup-button").text("OK");
                $("#popup-background").css("visibility", "visible").fadeIn(500);
                return;
            }
            else {
                $("#popup-title").text("Success").css("color", "green");
                $("#popup-message").text("Successfully added product to shopping cart");
                $("#popup-button").text("Continue shopping");
                $("#popup-background").css("visibility", "visible").fadeIn(500);
            }

            if ($("#scrollable").children().length == 0) {
                $("#cartEmpty").toggleClass("hidden");
                $("#cartEntry").toggleClass("hidden");
                $("#cartTotal").toggleClass("hidden");
                $("#cartProductQuantity").toggleClass("hidden");
            }

            if ($("#cartItem" + parse.get("productId")).length)
                $("#scrollable").find("#cartItem" + parse.get("productId")).find("#quantity").text(parse.get("quantity") + " pcs");
            else {
                $("#scrollable").append('<div class="flex-row" id="cartItem' + parse.get("productId") + '" style="width: 100%; height: 80px; border: 1px solid #e3e3e3;">' +
                                            '<div class="cart-image" style="height: auto; width: 100px; background-color: inherit;">' +
                                                '<image src="/img/item/' + parse.get("photo") + '" style="height: auto; width: auto; max-width: 100px; max-height: 78px;"></image>' +
                                            '</div>' +
                                            '<div class="cart-description" style="width: 100%;">' +
                                                '<div th:inline="text" style="font-size: 14px;">' + parse.get("name") + '</div>' +
                                                '<div th:inline="text" id="quantity">' + parse.get("quantity") + ' pcs</div>' +
                                                '<div th:inline="text">' + parse.get("price") + '$</div>' +
                                            '</div>' +
                                        '</div>');
            }

            $("#totalPrice").text(parse.get("totalPrice") + "$");

            if (parseInt(parse.get("productsQuantity")) != 0)
                $("#cartProductQuantity").text(parseInt(parse.get("productsQuantity")) > 9 ? "9+" : parse.get("productsQuantity"));
        },
//        error: function () {
//        }
    })
}


function addProduct(productId) {
    let data = {
            productId: productId,
            quantity: 1
            //quantity: $("#" + productId).find("quantity").val()
    };

    $.ajax({
        type: 'POST',
        url: '../api/addProduct',
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            let parse = new Map(Object.entries(response));

            if (parse.get("error_message") != null) {
                $("#popup-title").text("Warning").css("color", "#ffae00");
                $("#popup-message").text(parse.get("error_message"));
                $("#popup-button").text("OK");
                $("#popup-background").css("visibility", "visible").fadeIn(500);
                return;
            }

            $("#cartItem" + parse.get("productId")).find("#quantity").text(parse.get("quantity"));
            $("#total").text(parse.get("totalPrice") + "$");
        },
//        error: function () {
//        }
    })
}

function removeProduct(productId) {
    let data = {
            productId: productId,
            quantity: 1
            //quantity: $("#" + productId).find("quantity").val()
    };

    $.ajax({
        type: 'POST',
        url: '../api/removeProduct',
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            let parse = new Map(Object.entries(response));

            if (parse.get("error_message") != null) {
                $("#popup-title").text("Warning").css("color", "#ffae00");
                $("#popup-message").text(parse.get("error_message"));
                $("#popup-button").text("OK");
                $("#popup-background").css("visibility", "visible").fadeIn(500);
                return;
            }

            if (parseInt(parse.get("quantity")) != 0)
                $("#cartItem" + parse.get("productId")).find("#quantity").text(parse.get("quantity"));
            else
                $("#cartItem" + parse.get("productId")).remove();

            $("#total").text(parse.get("totalPrice") + "$");

            if (parseInt(parse.get("totalPrice")) == 0) {
                //window.location.replace("/");
                $("#sidePanel").toggleClass("hidden");
                $("#cartEmpty").toggleClass("hidden");
            }
        },
//        error: function () {
//        }
    })
}

function removeWholeProduct(productId) {
    let data = {
            productId: productId
            //quantity: $("#" + productId).find("quantity").val()
    };

    $.ajax({
        type: 'POST',
        url: '../api/removeWholeProduct',
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        success: function (response) {
            let parse = new Map(Object.entries(response));

            if (parse.get("error_message") != null) {
                $("#popup-title").text("Warning").css("color", "#ffae00");
                $("#popup-message").text(parse.get("error_message"));
                $("#popup-button").text("OK");
                $("#popup-background").css("visibility", "visible").fadeIn(500);
                return;
            }

            $("#cartItem" + parse.get("productId")).remove();
            $("#total").text(parse.get("totalPrice") + "$");

            if (parseInt(parse.get("totalPrice")) == 0) {
                //window.location.replace("/");
                $("#sidePanel").toggleClass("hidden");
                $("#cartEmpty").toggleClass("hidden");
            }
        },
//        error: function () {
//        }
    })
}