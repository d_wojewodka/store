package com.development.store.service.impl;

import com.development.store.repository.DiscountRepository;
import com.development.store.service.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiscountServiceImpl implements DiscountService {
// Public:
    public DiscountServiceImpl() {}
    public DiscountServiceImpl(final DiscountRepository discountRepository) {
        this.discountRepository = discountRepository;
    }

// Private:
    @Autowired private DiscountRepository discountRepository;
}
