package com.development.store.model;

import com.development.store.constants.GLOBAL;

import javax.persistence.*;

@Entity
@Table(name = "role", schema = GLOBAL.SCHEMA_ACCOUNT, catalog = GLOBAL.SCHEMA_ACCOUNT)
public class Role {
// Public:
    public Role() {}
    public Role(final String name, final int power) {
        this.name = name;
        this.power = power;
    }

    public long getId() { return id; }
    public String getName() { return name; }
    public int getPower() { return power; }
    public void setId(final long id) { this.id = id; }
    public void setName(final String name) { this.name = name; }
    public void setPower(final int add) { this.power = add; }

    @Override
    public String toString() {
        return "Role:" + "\n" +
                "\tid=" + id + "\n" +
                "\tname=" + name + "\n" +
                "\tpower=" + power;

    }

// Private:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) private long id;
    @Column(name = "name", nullable = false, unique = true) private String name;
    @Column(name = "power", nullable = false) private int power;
}
