package com.development.store.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncoder {
// PUBLIC:
    public static String encode(final String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }

    public static boolean matches(final String raw_password, final String encoded_password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        return encoder.matches(raw_password, encoded_password);
    }
}
