package com.development.store;


import com.development.store.service.OrderService;
import com.development.store.service.impl.OrderServiceImpl;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class OrderTests {
// Public:
//    @Test
//    @org.junit.jupiter.api.Order(1)
//    public void save() {
//        // given
//        Order order = new Order();
//        order.setUserId(1);
//        order.setCartId(1);
//        order.setDeliveryType(1);
//        order.setDeliveryCredentials("CREDENTIALS");
//        order.setDeliveryAddress1("ADDRESS1");
//        order.setDeliveryAddress2("ADDRESS2");
//        order.setDeliveryPhone(0123456789L);
//        order.setDeliveryNote("NOTE");
//        order.setDeliveryWeight(new BigDecimal.ONE);
//        order.setDeliveryPrice(new BigDecimal.ONE);
//
//        Timestamp time = new Timestamp(LocalDateTime.now());
//        order.setDeliveryETA(time);
//        order.setTotalPrice(new BigDecimal.ONE);
//        order.setDiscount("CODE");
//        order.setFinalPrice(new BigDecimal.ONE);
//        order.setPayment(1);
//        order.setStatus(1);
//
//
//        // then
//        Mockito.lenient().when(orderReposiory.save(Mockito.any(Order.class))).thenReturn(order);
//        Order newOrder = orderService.save(order);
//
//        Assertions.assertNotNull(newOrder);
//        Assertions.assertEquals(1, newOrder.getId());
//        Assertions.assertEquals(1, newOrder.getUserId());
//        Assertions.assertEquals(1, newOrder.getDeliveryType());
//        Assertions.assertEquals("CREDENTIALS", newOrder.getDeliveryCredentials());
//
//
//        // given
//        Collection<Cart> carts = new ArrayList();
//        for (int i = 0; i < 5; i++) {
//            Cart entry = new Cart(0, i, 3+(3*i*i));
//            carts.add(entry);
//        }
//
//        // then
//        Assertions.assertNotNull(carts);
//
//    }

// Private:
    @InjectMocks
    private OrderService orderService = new OrderServiceImpl();
}
