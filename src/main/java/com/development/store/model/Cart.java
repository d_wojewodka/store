package com.development.store.model;

import com.development.store.constants.GLOBAL;
import com.development.store.dto.CartDto;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "cart", schema = GLOBAL.SCHEMA_DATA, catalog = GLOBAL.SCHEMA_DATA)
public class Cart {
// Public:
    public Cart() {};
    public Cart(long productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }
    public Cart(long userId, long productId, int quantity) {
        this.userId = userId;
        this.productId = productId;
        this.quantity = quantity;
    }

    public int getId() { return id; }
    public long getUserId() { return userId; }
    public long getProductId() { return productId; }
    public int getQuantity() { return quantity; }
    public Timestamp getCreateAt() { return createAt; }
    public Timestamp getUpdateAt() { return updateAt; }
    public void setId(final int id) { this.id = id; }
    public void setUserId(final long userId) { this.userId = userId; }
    public void setProductId(final long productId) { this.productId = productId; }
    public void setQuantity(final int quantity) { this.quantity = quantity; }
    public void setCreateAt(final Timestamp createAt) { this.createAt = createAt; }
    public void setUpdateAt(final Timestamp updateAt) { this.updateAt = updateAt; }

    public Cart getFromDto(CartDto cartDto, long userId) {
        this.userId = userId;
        productId = Objects.nonNull(cartDto.getProductId()) ? cartDto.getProductId() : null;
        quantity = Objects.nonNull(cartDto.getQuantity()) ? cartDto.getQuantity() : null;

        return this;
    }

    @Override
    public String toString() {
        return "Cart:" + "\n" +
                "\tid=" + id + "\n" +
                "\tuserId=" + userId + "\n" +
                "\tproductId=" + productId + "\n" +
                "\tquantity=" + quantity + "\n" +
                "\tcreateAt=" + createAt + "\n" +
                "\tupdateAt=" + updateAt;
    }

// Private:
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) private int id;

    @Column(name="user_id", nullable = false) private long userId;
    @Column(name="product_id", nullable = false) private long productId;
    @Column(name="quantity", nullable = false) private int quantity;

    @CreationTimestamp
    @Column(name="create_at", nullable = false, updatable = false) private Timestamp createAt;

    @UpdateTimestamp
    @Column(name="update_at") private Timestamp updateAt;
}