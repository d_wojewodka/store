package com.development.store.service.impl;

import com.development.store.service.RequestService;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;


@Service
public class RequestServiceImpl implements RequestService {
// Public:
    @Override public String getClientIp(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-Forwarded-For");
        if(StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }

        if(StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }

        if(StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
            if(LOCALHOST_IPV4.equals(ipAddress) || LOCALHOST_IPV6.equals(ipAddress)) {
                try {
                    InetAddress inetAddress = InetAddress.getLocalHost();
                    ipAddress = inetAddress.getHostAddress();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        }

        if(!StringUtils.isEmpty(ipAddress)
                && ipAddress.length() > 15
                && ipAddress.indexOf(",") > 0) {
            ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
        }

        return ipAddress;
    }
    @Override public boolean redirect(Authentication authentication, HttpServletRequest request) {
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return true;
        }

        return false;
    }
    @Override public boolean redirect(Authentication authentication, HttpServletRequest request, boolean appendLog) {
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return true;
        }

        return false;
    }

// Private:
    private final String LOCALHOST_IPV4 = "127.0.0.1";
    private final String LOCALHOST_IPV6 = "0:0:0:0:0:0:0:1";
//    private static final String[] HEADERS_TO_TRY = {
//            "X-Forwarded-For",
//            "Proxy-Client-IP",
//            "WL-Proxy-Client-IP",
//            "HTTP_X_FORWARDED_FOR",
//            "HTTP_X_FORWARDED",
//            "HTTP_X_CLUSTER_CLIENT_IP",
//            "HTTP_CLIENT_IP",
//            "HTTP_FORWARDED_FOR",
//            "HTTP_FORWARDED",
//            "HTTP_VIA",
//            "REMOTE_ADDR" };
}