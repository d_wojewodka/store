package com.development.store.service.impl;

import com.development.store.model.Role;
import com.development.store.repository.RoleRepository;
import com.development.store.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class RoleServiceImpl implements RoleService {
// Public:
    public RoleServiceImpl() {}
    public RoleServiceImpl(final RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role save(final Role role) {
        try {
            Role newRole = roleRepository.save(role);
            return newRole;
        }
        catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean delete(final Role role) {
        try {
            roleRepository.delete(role);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public Role getById(final long id) {
        Role role = roleRepository.getById(id);
        return Objects.nonNull(role) ? role : null;
    }

    @Override
    public Role getByName(final String name) {
        Role role = roleRepository.getByName(name);
        return Objects.nonNull(role) ? role : null;
    }

// Private:
    @Autowired private RoleRepository roleRepository;
}
