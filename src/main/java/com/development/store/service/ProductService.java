package com.development.store.service;

import com.development.store.dto.ProductDto;
import com.development.store.model.Product;

import java.util.Collection;

public interface ProductService {
    Product save(Product product);
    boolean delete(Product product);

    String checkProductQuantity(long productId, int quantity);
    Collection<ProductDto> entityToDto(Collection<Product> entity);

    Collection<Product> getAll();

    Product getById(long productId);
    Collection<Product> getByName(String name);
    Collection<Product> getByCategory(String filter);
    Collection<Product> search(String text);
}
