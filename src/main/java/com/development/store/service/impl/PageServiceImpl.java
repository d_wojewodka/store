package com.development.store.service.impl;

import com.development.store.model.Page;
import com.development.store.repository.PageRepository;
import com.development.store.service.AuthService;
import com.development.store.service.PageService;
import com.development.store.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.file.Paths;
import java.util.Objects;

@Service
public class PageServiceImpl implements PageService {
// Public:
    @Override
    public Page save(final Page page) {
        try {
            Page newPage = pageRepository.save(page);
            return newPage;
        }
        catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean delete(final Page page) {
        try {
            pageRepository.delete(page);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public String redirect(final String path) {
        getRequiredPower(path);
        int required_power = 0;
        String all = (Paths.get(path).getParent() + "/**").replace("\\", "/");
        Page page = pageRepository.getByPage(all);
        if (Objects.nonNull(page)) {
            required_power = page.getRequiredPower();
        }

        page = pageRepository.getByPage(path);
        if (Objects.nonNull(page)) {
            required_power = page.getRequiredPower() >= required_power ? required_power : page.getRequiredPower();
        }

        if (required_power > 0) {
            if (authService.isLoggedIn()) {
                if (userService.getRolePower() >= required_power)
                    return path;

                return "/access/denied";
            }

            return "/user/login";
        }

        return path;
    }

    @Override
    public int getRequiredPower(final String path) {
        int requiredPower = 0;
        Page page = pageRepository.getByPage(path);

        // exact path
        if (Objects.nonNull(page))
            requiredPower = page.getRequiredPower();

        // path exclusion
//        System.err.println("start read : " + path);
        if (path.startsWith("/")) {
            String[] array = path.split("/");
//            System.err.println(array.length);
            String search = "";
            for (int i = 0; i < array.length; i++) {
                Page directory, directories;
                if (i == array.length) {
                    directory = pageRepository.getByPage(search.length() > 0 ? search : "/");
                }

                directory = pageRepository.getByPage(search + "*");
                if (Objects.nonNull(directory)) {

                }

                directories = pageRepository.getByPage(search + "**");
                if (Objects.nonNull(directories)) {}


                    search += "/" + array[i];
            }

            return requiredPower;
        }

        return requiredPower;
    }

    @Override
    public Page getById(long id) {
        Page page = pageRepository.getById(id);
        return Objects.nonNull(page) ? page : null;
    }

    @Override
    public Page getByPage(String path) {
        Page page = pageRepository.getByPage(path);
        return Objects.nonNull(page) ? page : null;
    }

    // Private:
    @Autowired private AuthService authService;
    @Autowired private UserService userService;
    @Autowired private PageRepository pageRepository;
}
