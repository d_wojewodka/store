package com.development.store.service;

import com.development.store.dto.CartDto;
import com.development.store.model.Cart;

import java.math.BigDecimal;
import java.util.Collection;

public interface CartService {
    Cart save(Cart cart);
    Collection<Cart> saveAll(Collection<Cart> carts);
    boolean delete(Cart cart);

    boolean addProduct(long productId, int quantity);
    boolean removeProduct(long productId, int quantity);

    Collection<CartDto> entityToDto(Collection<Cart> entity);

    Collection<Cart> mergeCart(Collection<Cart> cart);

    int getCartItemCount(long productId);
    int getProductQuantity();
    BigDecimal getTotal(Collection<Cart> cart);
    Collection<Cart> getCartItems();

    Cart changeQuantity(long userId, long productId, int quantity);

    Cart getByProduct(long productId, long userId);
    Collection<Cart> getByUser(long userId);
}
