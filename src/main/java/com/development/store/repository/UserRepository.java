package com.development.store.repository;

import com.development.store.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {
    @Query("SELECT r FROM User r WHERE r.username = :username")
        User getByUsername(@Param("username") String username);
    @Query("SELECT r FROM User r WHERE r.email = :email")
        User getByEmail(@Param("email") String email);
}

