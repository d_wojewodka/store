package com.development.store.config;

import com.development.store.service.impl.UserDetailsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Configuration @EnableWebSecurity public class WebSecurittyConfig extends WebSecurityConfigurerAdapter {
// Public:
    @Bean public org.springframework.security.core.userdetails.UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    }
    @Bean public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    @Bean public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsService());
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());

        return daoAuthenticationProvider;
    }

// Protected:
    @Override protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }
    @Override protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            // Roles
            .and()
                .authorizeRequests()
                //.antMatchers("/view/**").hasAnyAuthority("USER", "ADMIN", "DEV")
                //.antMatchers("/new/**").hasAnyAuthority("ADMIN", "DEV")
                //.antMatchers("/edit/**").hasAnyAuthority("ADMIN", "DEV")
                //.antMatchers("/delete/**").hasAuthority("DEV")
                //.antMatchers("/admin/**")
                    //.hasAnyRole("DEVELOPER", "ADMIN")
                    //.anyRequest()
                    //.permitAll()
            // Login & Logout
            .and()
                .formLogin()
                    .loginPage("/user/login")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .defaultSuccessUrl("/home")
                    //.successHandler(loginSuccessHandler)
                    //.failureHandler(loginFailureHandler)
                    .permitAll()
            .and()
                .logout()
                    .logoutUrl("/user/logout")
                    .logoutSuccessUrl("/user/login?logout")
                    //.logoutSuccessHandler(logoutSuccessHandler)
                    .permitAll()
            .and()
                .csrf().disable()
                .rememberMe().key("AbcdEfghIjklmNopQrsTuvXyz_0123456789");
    }
// Private:
}
