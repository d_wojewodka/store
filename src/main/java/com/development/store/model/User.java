package com.development.store.model;

import com.development.store.constants.GLOBAL;
import com.development.store.dto.UserDto;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user", schema = GLOBAL.SCHEMA_ACCOUNT, catalog = GLOBAL.SCHEMA_ACCOUNT)
public class User {
// Public:
    public static final int MINIMUM_USERNAME_LENGTH = 3;
    public static final int MINIMUM_PASSWORD_LENGTH = 3;

    public User() {}
    public User(final String username, final String password, final String email) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.role = "USER";
        this.enabled = false;
        this.locked = false;
    }

    public long getId() { return id; }
    public String getUsername() { return username; }
    public String getPassword() { return password; }
    public String  getEmail() { return email; }
    public String getRole() { return role; }
    public Boolean isEnabled() { return enabled; }
    public Boolean isLocked() { return locked; }

    public void setId(final long id) { this.id = id; }
    public void setUsername(final String username) { this.username = username; }
    public void setPassword(final String password) { this.password = password; }
    public void setEmail(final String email) { this.email = email; }
    public void setRole(final String role) { this.role = role; }
    public void setEnabled(final Boolean enabled) { this.enabled = enabled; }
    public void setLocked(final Boolean locked) { this.locked = locked; }

    public User getFromDto(UserDto userDto) {
        id = Objects.nonNull(userDto.getUserId()) ? userDto.getUserId() : null;
        username = Objects.nonNull(userDto.getUsername()) ? userDto.getUsername() : null;
        password = Objects.nonNull(userDto.getPassword()) ? userDto.getPassword() : null;
        email = Objects.nonNull(userDto.getEmail()) ? userDto.getEmail() : null;
        role = "USER";
        enabled = false;
        locked = false;

        return this;
    }

    public static String encryptPassword(final String password) {
        BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();
        return bCrypt.encode(password);
    }

    @Override
    public String toString() {
        return "User:" + "\n" +
                "\tid=" + id + "\n" +
                "\tusername=" + username + "\n" +
                "\tpassword=" + password + "\n" +
                "\temail=" + email + "\n" +
                "\trole=" + role + "\n" +
                "\tenabled=" + enabled + "\n" +
                "\tlocked=" + locked;
    }

// Private:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) private long id;
    @Column(name="username", nullable = false, unique = true) private String username;
    @Column(name="password", nullable = false) private String password;
    @Column(name="email", nullable = false, unique = true) private String email;
    @Column(name="role") private String role;
    @Column(name="enabled") private Boolean enabled;
    @Column(name="locked") private Boolean locked;
}
