package com.development.store.service.impl;

import com.development.store.dto.CartDto;
import com.development.store.dto.ProductDto;
import com.development.store.model.Cart;
import com.development.store.model.Product;
import com.development.store.repository.CartRepository;
import com.development.store.repository.ProductRepository;
import com.development.store.repository.UserRepository;
import com.development.store.service.CartService;
import com.development.store.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Objects;

@Service
public class CartServiceImpl implements CartService {
// Public:
    public CartServiceImpl() {}
    public CartServiceImpl(final CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    @Override
    public Collection<Cart> saveAll(final Collection<Cart> carts) {
        try {
            Collection<Cart> newCart = cartRepository.saveAll(carts);
            return newCart;
        }
        catch (Exception e) {
            return null;
        }
    }

    @Override
    public Cart save(final Cart cart) {
        try {
            Cart newCart = cartRepository.save(cart);
            return newCart;
        }
        catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean delete(final Cart cart) {
        try {
            cartRepository.delete(cart);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override public boolean addProduct(final long productId, final int quantity) {
        if (quantity < 0)
            // quantity < 0, eg. -1
            return false;

        Cart current;
        try {
            // user
            current = cartRepository.getByProduct(productId, userRepository.getByUsername(((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).getId());
        } catch (ClassCastException e) {
            // guest
            if (guestCart.size() != 0) {
                if (guestCart.containsKey(productId)) {
                    Cart cart = guestCart.get(productId);
                    cart.setQuantity(cart.getQuantity() + quantity);
                    guestCart.replace(productId, cart);
                    return true;
                }
            }

            guestCart.put(productId, new Cart(productId, quantity));

            return true;
        }
        Cart cartItem = current;

        if (cartItem != null) {
            // loading product quantity
            // current user already have this item in cart
            cartItem.setQuantity(cartItem.getQuantity() + quantity);
        }
        else {
            // creating a new entry in database
            // current user does not have this product in cart
            cartItem = new Cart(userRepository.getByUsername(((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).getId(), productId, quantity);

        }

        // compare changes "current" with "cartItem"
        cartRepository.save(cartItem);
        return true;
    }

    @Override public boolean removeProduct(final long productId, final int quantity) {
        // validate user

        if (quantity < 0)
            // quantity < 0, eg. -1
            return false;

        Cart current;
        try {
            // user
            current = cartRepository.getByProduct(productId, userRepository.getByUsername(((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).getId());
        } catch (ClassCastException e) {
            // guest
            if (guestCart.size() != 0) {
                if (guestCart.containsKey(productId)) {
                    if ((guestCart.get(productId).getQuantity() - quantity) > 0) {
                        guestCart.get(productId).setQuantity(guestCart.get(productId).getQuantity() - quantity);
                        return true;
                    }

                    guestCart.remove(productId);
                    return true;
                }
                return false;
            }

            // guest cart is empty
            return false;
        }

        Cart cartItem = current;

        if (cartItem != null) {
            if (cartItem.getQuantity() > 0 && cartItem.getQuantity() - quantity > 0) {
                // updates entry in database
                // quantity > 0
                cartItem.setQuantity(cartItem.getQuantity() - quantity);
                cartRepository.save(cartItem);
                return true;
            }
            else {
                // removes entry in database
                // quantity <= 0
                cartRepository.delete(cartItem);
                return true;
            }
        }

        // entry does not exists in database
        // bug or exploit
        return false;
    }

    @Override public Collection<Cart> getCartItems() {
        Collection<Cart> cartItems = new ArrayList();

        try {
            cartItems = cartRepository.getByUser(userService.getId());

            if (guestCart.size() != 0)
                cartItems = this.mergeCart(cartItems);

        } catch (ClassCastException e) {
            for (HashMap.Entry<Long, Cart> entry : guestCart.entrySet()) {
                cartItems.add(entry.getValue());
            }

            return cartItems;
        }

        return cartItems;
    }

    @Override
    public Cart changeQuantity(final long userId, final long productId, final int quantity) {
        Cart result = getByProduct(productId, userId);
        result.setQuantity(result.getQuantity() + quantity);

        if (result.getQuantity() <= 0) {
            return save(result);
        }
        else
            return save(result);
    }

    @Override
    public Cart getByProduct(final long productId, final long userId) {
        Cart cart = cartRepository.getByProduct(productId, userId);
        return Objects.nonNull(cart) ? cart : null;
    }

    @Override
    public Collection<Cart> getByUser(final long userId) {
        Collection<Cart> carts = cartRepository.getByUser(userId);
        return Objects.nonNull(carts) ? carts : null;
    }

    @Override public Collection<CartDto> entityToDto(final Collection<Cart> entity) {
        Collection <CartDto> dto = new ArrayList();

        for (Cart cart : entity) {
            dto.add(new CartDto(new ProductDto().getFromEntity(productRepository.getById(cart.getProductId())), cart.getQuantity()));
        }

        return dto;
    }

    @Override public int getCartItemCount(final long productId) {
        Cart cartItem;
        try {
            cartItem = cartRepository.getByProduct(productId, userRepository.getByUsername(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).getId());
        } catch (ClassCastException e) {
            if (guestCart.containsKey(productId))
                return guestCart.get(productId).getQuantity();

            return 0;
        }

        if (cartItem != null)
            // if exists in database
            // returns collection of it
            return cartItem.getQuantity();

        // if not exists in database
        // returns null
        return 0;
    }

    @Override public BigDecimal getTotal(final Collection<Cart> cart) {
        Collection<Cart> cartItems;
        try {
            cartItems = cartRepository.getByUser(userRepository.getByUsername(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).getId());
        } catch (ClassCastException e) {
            cartItems = cart;
        }

        BigDecimal sum = new BigDecimal(0);

        if (cartItems != null) {
            for (Cart entry : cartItems) {
                Product product = productRepository.getById(entry.getProductId());

                if (product != null)
                    // product exists in database
                    // calculate sum
                    sum = sum.add((product.getPrice().multiply(new BigDecimal(entry.getQuantity()))));
            }

            // if exists in database
            // returns sum of cart item quantity multiplied by price
            return sum;
        }

        // if not exists in database
        // returns BigDecimal.ZERO = 0
        return sum;
    }

    @Override public int getProductQuantity() {
        Collection<Cart> cartItems;
        try {
            cartItems = cartRepository.getByUser(userRepository.getByUsername(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).getId());
        } catch (ClassCastException e) {
            return guestCart.size();
        }

        if (cartItems != null) {
            // if exists in database
            // returns size of it
            return cartItems.size();
        }

        // if not exists in database
        // returns 0
        return 0;
    }

    @Override public Collection<Cart> mergeCart(final Collection<Cart> cart) {
        Collection<Cart> merge = new ArrayList();

        long userId = userRepository.getByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getId();

        if (cart.size() != 0) {
            for (Cart entry : cart) {
                if (guestCart.containsKey(entry.getProductId())) {
                    entry.setQuantity(entry.getQuantity() + guestCart.get(entry.getProductId()).getQuantity());
                    guestCart.remove(entry.getProductId());
                    merge.add(entry);
                }
                else {
                    merge.add(entry);
                }
            }

            for (HashMap.Entry<Long, Cart> entry : guestCart.entrySet()) {
                Cart guest = entry.getValue();
                guest.setUserId(userId);
                merge.add(guest);
                //merge.add(new Cart(userId, entry.getValue().getProductId(), entry.getValue().getQuantity()));
            }

            guestCart.clear();
        }
        else {
            for (HashMap.Entry<Long, Cart> entry : guestCart.entrySet()) {
                Cart guest = entry.getValue();
                guest.setUserId(userId);
                merge.add(guest);
            }

            guestCart.clear();
        }

        cartRepository.saveAll(merge);
        return merge;
    }

// Private:
    @Autowired private CartRepository cartRepository;
    @Autowired private ProductRepository productRepository;
    @Autowired private UserRepository userRepository;
    @Autowired private UserService userService;
    private HashMap<Long, Cart> guestCart = new HashMap();
}
