package com.development.store.service.impl;

import com.development.store.model.Address;
import com.development.store.model.Cart;
import com.development.store.model.Role;
import com.development.store.model.User;
import com.development.store.repository.AddressRepository;
import com.development.store.repository.CartRepository;
import com.development.store.repository.RoleRepository;
import com.development.store.repository.UserRepository;
import com.development.store.service.AuthService;
import com.development.store.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Objects;

@Service
public class UserServiceImpl implements UserService {
    public UserServiceImpl() {}
    public UserServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User save(final User user) {
        try {
            User newUser = userRepository.save(user);
            return newUser;
        }
        catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean delete(final User user) {
        try {
            userRepository.delete(user);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public Collection<User> getAll() {
        Collection<User> users = userRepository.findAll();
        return Objects.nonNull(users) ? users : null;
    }
    @Override
    public User getByUsername(final String username) {
        User user = userRepository.getByUsername(username);
        return Objects.nonNull(user) ? user : null;
    }

    @Override
    public User getByEmail(final String email) {
        User user = userRepository.getByUsername(email);
        return Objects.nonNull(user) ? user : null;
    }

    @Override
    public User getById(final long userId) {
        User user = userRepository.getById(userId);
        return Objects.nonNull(user) ? user : null;
    }

    @Override
    public boolean duplicationUsername(final String username) {
        User user = userRepository.getByUsername(username);
        return Objects.nonNull(user) ? true : false;
    }

    @Override
    public boolean duplicationEmail(final String email) {
        User user = userRepository.getByEmail(email);
        return Objects.nonNull(user) ? true : false;
    }

    @Override
    public String encryptPassword(final String raw_password) {
        return bCryptPasswordEncoder.encode(raw_password);
    }

    @Override
    public boolean validatePassword(final String raw_password, final String encrypted_password) {
        return (raw_password.length() + encrypted_password.length()) > 0 ? bCryptPasswordEncoder.matches(raw_password, encrypted_password) : false;
    }

    @Override
    public User changePassword(final User user, final String current_password, final String new_password) {
        if (Objects.nonNull(user)) {
            if (validatePassword(current_password, user.getPassword())) {
                user.setPassword(encryptPassword(new_password));
                return user;
            }
            return user;
        }

        return null;
    }

    @Override
    public Address getAddress(final long userId) {
        Address address = addressRepository.getByUserId(userId);
        return Objects.nonNull(address) ? address : null;
    }

    @Override
    public Collection<Cart> getCart(final long userId) {
        Collection<Cart> cart = cartRepository.getByUser(userId);
        return Objects.nonNull(cart) ? cart : null;
    }

    @Override
    public int getRolePower() {
        if (authService.isLoggedIn()) {
            try {
                User user = userRepository.getByUsername(authService.getName());
                Role role = roleRepository.getByName(user.getRole());
                return role.getPower();
            }
            catch (Exception e) {
                return 0;
            }
        }

        return 0;
    }

    @Override
    public long getId() {
        if (authService.isLoggedIn()) {
            User user = getByUsername(authService.getName());
            return Objects.nonNull(user) ? user.getId() : 0;
        }

        return 0;
    }

    // Private:
    @Autowired private AuthService authService;
    @Autowired private UserRepository userRepository;
    @Autowired private AddressRepository addressRepository;
    @Autowired private CartRepository cartRepository;
    @Autowired private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
}
