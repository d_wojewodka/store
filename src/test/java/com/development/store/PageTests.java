package com.development.store;

import com.development.store.model.Page;
import com.development.store.repository.PageRepository;
import com.development.store.service.PageService;
import com.development.store.service.impl.PageServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PageTests {
// Public:
    @Test @Order(1)
    public void save() {
        Page page = new Page();
        page.setId(1);

        Mockito.lenient().when(pageRepository.save(Mockito.any(Page.class))).thenReturn(page);
        Page newPage = pageService.save(page);

        Assertions.assertNotNull(newPage);
        Assertions.assertEquals(1, newPage.getId());

        page.setPage("/**");
        page.setRequiredPower(10);

        newPage = pageService.save(page);

        Assertions.assertEquals("/**", page.getPage());
        Assertions.assertEquals(10, page.getRequiredPower());
    }

    @Test @Order(2)
    public void get() {
        Page page = new Page();
        page.setId(1);
        page.setPage("/**");

        Mockito.lenient().when(pageRepository.save(Mockito.any(Page.class))).thenReturn(page);
        Mockito.lenient().when(pageRepository.getById(Mockito.any(long.class))).thenReturn(page);
        Mockito.lenient().when(pageRepository.getByPage(Mockito.any(String.class))).thenReturn(page);

        Page newPage = pageService.save(page);

        Assertions.assertNotNull(pageService.getById(newPage.getId()));
        Assertions.assertNotNull(pageService.getByPage(newPage.getPage()));
    }

// Private:
    @InjectMocks
    PageService pageService = new PageServiceImpl();

    @Mock
    PageRepository pageRepository;
}
