var displayedContainer = "";

// updated to jQuery
function smartController(name) {
    if (displayedContainer == "") {
        $("." + name).fadeIn(200);
        $("." + name).removeClass("hidden");
        displayedContainer = name;
    } else {
        if (displayedContainer == name) {
            $("." + name).fadeOut(200).addClass("hidden");
            displayedContainer = "";
        }
        else {
            $("." + displayedContainer).fadeOut(200).addClass("hidden");
            $("." + name).fadeIn(200).removeClass("hidden");
            displayedContainer = name;
        }
    }
};


function openPopup(ID) {
    $("#" + ID).css("visibility", "visible").fadeIn(100);
};

function closePopup(ID) {
    $("#" + ID).css("visibility", "hidden").delay(100);
};

// usage:
// onclick="toggleInputType('password-input-id', 'toggle-button-id')"
function toggleInputType(field, toggler) {
    // assign id to variable
	var password_field = document.querySelector("#" + field);
	var toggle_image = document.querySelector("#" + toggler);

    var type = password_field.getAttribute("type");

	if (password_field.value.length > 0) {
	    // toggle the icon

        toggle_image.classList.remove(type == "password" ? "password-hide" : "password-view");
        toggle_image.classList.add(type == "password" ? "password-view" : "password-hide");
        password_field.setAttribute("type", (type === "password" ? "text" : "password"));
	}
	else {
	    if (type == "text") {
	    	// toggle type
	        password_field.setAttribute("type", "password");

	        // toggle the icon
	        toggle_image.classList.remove("password-hide");
	        toggle_image.classList.add("password-view");
	    }
	}
};

function clearSearchInput() {
    $("#searchInput").val("");
    $("#searchInput").focus();
};

function search() {
    if ($("#searchInput").val().length != 0)
        document.getElementById('searchForm').submit();
};

//
//const isEmpty = str => !str.trim().length();
//
//document.getElementById("searchInput").addEventListener("input", function() {
//    if (isEmpty(this.value))
//        console.log("empty");
//    else
//        console.log("contains text");
//});



function toggleSpoiler(contentId, imageId) {
	var content = document.querySelector("#" + contentId);
	var image = document.querySelector("#" + imageId);

    var type = image.getAttribute("class");

	if (image.classList.contains("button_arrow_down")) {
	    content.classList.toggle("hidden");
	    image.classList.remove("button_arrow_down");
	    image.classList.add("button_arrow_up");
	}
	else {
        content.classList.add("hidden");
        image.classList.remove("button_arrow_up");
        image.classList.add("button_arrow_down");
	}
};

function toggleVisibility(elementId) {
    var forms = ["#addressName",
                "#addressLastName",
                "#address1",
                "#address2",
                "#addressCity",
                "#addressPostalCode",
                "#addressCountry",
                "#addressPhone"];

    forms.forEach(value => { $(value).removeClass("error").val(""); });

    var element = document.querySelector("#" + elementId);
    element.classList.toggle("visible");
};

function validateFields(id) {
    if ($(id).val().length == 0)$(id).addClass("error");
    else $(id).removeClass("error");
}

function deliveryAddress() {
    var forms = ["#addressName",
                "#addressLastName",
                "#address1",
                "#address2",
                "#addressCity",
                "#addressPostalCode",
                "#addressCountry",
                "#addressPhone"];

    forms.forEach(validateFields);

    if ($("#address").find(".error").length != 0)
        return;

    $("#addressLine1").text($("#addressName").val() + " " + $("#addressLastName").val());
    $("#addressLine2").text($("#address1").val() + " " + $("#address2").val());
    $("#addressLine3").text($("#addressCity").val() + " " + $("#addressPostalCode").val() + ", " + $("#addressCountry").val());
    $("#phoneNumber").text($("#addressPhone").val());

    toggleVisibility("address");

};






function togglePasswordVisibility(inputId, imageId) {
    var input = document.querySelector("#" + inputId);
    var image = document.querySelector("#" + imageId);
    let type = input.getAttribute("type");

    if (input.value.length > 0) {
        // toggle the icon
        image.classList.remove(type == "password" ? "button_hide_password" : "button_show_password");
        image.classList.add(type == "password" ? "button_show_password" : "button_hide_password");
        input.setAttribute("type", (type === "password" ? "text" : "password"));
    }
    else {
        if (type == "password") {
            // toggle type
            input.setAttribute("type", "password");

            // toggle the icon
            image.classList.remove("button_show_password");
            image.classList.add("button_hide_password");
        }
    }

}