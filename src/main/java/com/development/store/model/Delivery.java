package com.development.store.model;

import com.development.store.constants.GLOBAL;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "delivery", schema = GLOBAL.SCHEMA_DATA, catalog = GLOBAL.SCHEMA_DATA)
public class Delivery {
// Public
    public Long getId() { return id; }
    public String getName() { return name; }
    public BigDecimal getPrice() { return price; }
    public boolean getAvailable() { return available; }

    public void setId(final long id) { this.id = id; }
    public void setName(final String name) { this.name = name; }
    public void setPrice(final BigDecimal price) { this.price = price; }
    public void setAvailable(final boolean available) { this.available = available; }

// Private
    @Id
    @Column(name = "id", nullable = false) private long id;
    @Column(name = "name", nullable = false, unique = true) private String name;
    @Column(name = "price", nullable = false) private BigDecimal price;
    @Column(name = "available", nullable = false) private boolean available;
}
