package com.development.store.dto;

import com.development.store.model.Order;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class OrderDto {
    OrderDto() {}
    OrderDto(Order order) {
        userId = order.getUserId();
        cartId = order.getCartId();
        deliveryType = order.getDeliveryType();
        deliveryCredentials = order.getDeliveryCredentials();
        deliveryAddress1 = order.getDeliveryAddress1();
        deliveryAddress2 = order.getDeliveryAddress2();
        deliveryPhone = order.getDeliveryPhone();
        deliveryNote = order.getDeliveryNote();
        deliveryWeight = order.getDeliveryWeight();
        deliveryPrice = order.getDeliveryPrice();
        deliveryETA = order.getDeliveryETA();
        totalPrice = order.getDeliveryPrice();
        discount = order.getDiscount();
        finalPrice = order.getFinalPrice();
        payment = order.getPayment();
        status = order.getStatus();
        createAt = order.getCreateAt();
    }

    public long getUserId() { return userId; }
    public long getCartId() { return cartId; }
    public int getDeliveryType() { return deliveryType; }
    public String getDeliveryCredentials() { return deliveryCredentials; }
    public String getDeliveryAddress1() { return deliveryAddress1; }
    public String getDeliveryAddress2() { return deliveryAddress2; }
    public Long getDeliveryPhone() { return deliveryPhone; }
    public String getDeliveryNote() { return deliveryNote; }
    public BigDecimal getDeliveryWeight() { return deliveryWeight; }
    public BigDecimal getDeliveryPrice() { return deliveryPrice; }
    public Timestamp getDeliveryETA() { return deliveryETA; }
    public BigDecimal getTotalPrice() { return totalPrice; }
    public String getDiscount() { return discount; }
    public BigDecimal getFinalPrice() { return finalPrice; }
    public int getPayment() { return payment; }
    public int getStatus() { return status; }
    public Timestamp getCreateAt() { return createAt; }

    public void setUserId(final long userId) { this.userId = userId; }
    public void setCartId(final long cartId) { this.cartId = cartId; }
    public void setDeliveryType(final int deliveryType) { this.deliveryType = deliveryType; }
    public void setDeliveryCredentials(final String deliveryCredentials) { this.deliveryCredentials = deliveryCredentials; }
    public void setDeliveryAddress1(final String deliveryAddress1) { this.deliveryAddress1 = deliveryAddress1; }
    public void setDeliveryAddress2(final String deliveryAddress2) { this.deliveryAddress2 = deliveryAddress2; }
    public void setDeliveryPhone(final Long deliveryPhone) { this.deliveryPhone = deliveryPhone; }
    public void setDeliveryNote(final String deliveryNote) { this.deliveryNote = deliveryNote; }
    public void setDeliveryWeight(final BigDecimal deliveryWeight) { this.deliveryWeight = deliveryWeight; }
    public void setDeliveryPrice(final BigDecimal deliveryPrice) { this.deliveryPrice = deliveryPrice; }
    public void setDeliveryETA(final Timestamp deliveryETA) { this.deliveryETA = deliveryETA; }
    public void setTotalPrice(final BigDecimal totalPrice) { this.totalPrice = totalPrice; }
    public void setDiscount(final String discount) { this.discount = discount; }
    public void setFinalPrice(final BigDecimal finalPrice) { this.finalPrice = finalPrice; }
    public void setPayment(final int payment) { this.payment = payment; }
    public void setStatus(final int status) { this.status = status; }
    public void setCreateAt(final Timestamp createAt) { this.createAt = createAt; }

    private long userId;
    private long cartId;
    private int deliveryType;
    private String deliveryCredentials;
    private String deliveryAddress1;
    private String deliveryAddress2;
    private Long deliveryPhone;
    private String deliveryNote;
    private BigDecimal deliveryWeight;
    private BigDecimal deliveryPrice;
    private Timestamp deliveryETA;
    private BigDecimal totalPrice;
    private String discount;
    private BigDecimal finalPrice;
    private int payment;
    private int status;
    private Timestamp createAt;
}
