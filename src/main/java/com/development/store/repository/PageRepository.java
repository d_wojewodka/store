package com.development.store.repository;

import com.development.store.model.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PageRepository extends JpaRepository<Page, Long> {
    @Query("SELECT r FROM Page r WHERE r.page = :page")
    Page getByPage(@Param("page") String page);
}
