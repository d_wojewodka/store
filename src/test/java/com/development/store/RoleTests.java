package com.development.store;

import com.development.store.model.Role;
import com.development.store.repository.RoleRepository;
import com.development.store.service.RoleService;
import com.development.store.service.impl.RoleServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class RoleTests {
// Public:
    @Test
    @Order(1)
    public void save() {
        // given
        Role role = new Role();
        role.setId(1);

        // then
        Mockito.lenient().when(roleRepository.save(Mockito.any(Role.class))).thenReturn(role);

        Assertions.assertNotNull(role);

        // given
        role.setName("NAME");
        role.setPower(10);

        Role newRole = roleService.save(role);
        Assertions.assertNotNull(role);

        Assertions.assertEquals(1, newRole.getId());
        Assertions.assertEquals("NAME", newRole.getName());
        Assertions.assertEquals(10, newRole.getPower());
    }

    @Test
    @Order(2)
    public void get() {
        // given
        Role role = new Role();
        role.setId(1);
        role.setName("NAME");

        // then
        Mockito.lenient().when(roleRepository.save(Mockito.any(Role.class))).thenReturn(role);
        Mockito.lenient().when(roleRepository.getById(Mockito.any(long.class))).thenReturn(role);
        Mockito.lenient().when(roleRepository.getByName(Mockito.any(String.class))).thenReturn(role);

        Assertions.assertNotNull(roleService.getById(1));
        Assertions.assertNotNull(roleService.getByName("NAME"));
    }

// Private:
    @InjectMocks
    private RoleService roleService = new RoleServiceImpl();

    @Mock
    private RoleRepository roleRepository;
}
