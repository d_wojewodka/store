package com.development.store.service;

import com.development.store.model.Page;

public interface PageService {
    Page save(Page page);
    boolean delete(Page page);

    String redirect(String path);
    int getRequiredPower(String path);

    Page getById(long id);
    Page getByPage(String path);
}
