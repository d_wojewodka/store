package com.development.store.repository;

import com.development.store.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("SELECT r FROM Product r WHERE r.id = :id")
        Product getById(@Param("id") String id);
    @Query("SELECT r FROM Product r WHERE r.name = :name")
        Collection<Product> getByName(@Param("name") String name);
    @Query("SELECT r FROM Product r WHERE r.category = :category")
        Collection<Product> getByCategory(@Param("category") String category);

    @Query("SELECT r FROM Product r WHERE " +
            "r.name LIKE %:search% " +
            "OR r.category LIKE %:search% " +
//            "OR r.description LIKE %:search% " +
            "OR r.manufacturer LIKE %:search% " +
            "OR r.collection LIKE %:search% ")
        Collection<Product> getByPhrase(@Param("search") String search);
}
