package com.development.store;

import com.development.store.model.User;
import com.development.store.repository.UserRepository;
import com.development.store.service.UserService;
import com.development.store.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserTests {
// Public:
    @Test @Order(1)
    public void save() {
        // given
        User user = new User();
        user.setId(1);

        // then
        Mockito.lenient().when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
        User newUser = userService.save(user);

        Assertions.assertNotNull(newUser);
        Assertions.assertEquals(1, newUser.getId());

        // given
        user.setUsername("USERNAME");
        user.setPassword(userService.encryptPassword("PASSWORD"));
        user.setEmail("EMAIL");
        user.setRole("USER");
        user.setEnabled(true);
        user.setLocked(false);

        // then
        newUser = userService.save(user);

        Assertions.assertNotNull(newUser);
        Assertions.assertEquals(1, newUser.getId());
        Assertions.assertEquals("USERNAME", newUser.getUsername());
        Assertions.assertEquals(true, userService.validatePassword("PASSWORD", newUser.getPassword()));
        Assertions.assertEquals("EMAIL", newUser.getEmail());
        Assertions.assertEquals("USER", newUser.getRole());
        Assertions.assertEquals(true, newUser.isEnabled());
        Assertions.assertEquals(false, newUser.isLocked());
    }

    @Test @Order(2)
    public void get() {
        // given
        User user = new User();
        user.setId(1);
        user.setUsername("USERNAME");
        user.setEmail("USERNAME");

        // then
        Mockito.lenient().when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
        Mockito.lenient().when(userRepository.getById(Mockito.any(long.class))).thenReturn(user);
        Mockito.lenient().when(userRepository.getByUsername(Mockito.any(String.class))).thenReturn(user);
        Mockito.lenient().when(userRepository.getByEmail(Mockito.any(String.class))).thenReturn(user);
        User newUser = userService.save(user);

        Assertions.assertNotNull(newUser);
        Assertions.assertNotNull(userService.getById(newUser.getId()));
        Assertions.assertNotNull(userService.getByUsername(newUser.getUsername()));
        Assertions.assertNotNull(userService.getByEmail(newUser.getEmail()));
    }

    @Test @Order(3)
    public void changePassword() {
        // given
        User user = new User();
        user.setId(1);
        user.setPassword(userService.encryptPassword("PASSWORD"));

        // then
        Mockito.lenient().when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
        User newUser = userService.save(user);

        Assertions.assertNotNull(newUser);
        Assertions.assertEquals(true, userService.validatePassword("PASSWORD", newUser.getPassword()));

        // given
        user = userService.changePassword(user, "PASSWORD", "password");
        newUser = userService.save(user);

        // then
        Assertions.assertNotNull(newUser);
        Assertions.assertEquals(true, userService.validatePassword("password", newUser.getPassword()));
        Assertions.assertNotEquals(true, userService.validatePassword("PASSWORD", newUser.getPassword()));
    }

// Private:
    @InjectMocks
    private UserService userService = new UserServiceImpl();

    @Mock
    private UserRepository userRepository;

}