package com.development.store.controller;

import com.development.store.dto.CartDto;
import com.development.store.model.Product;
import com.development.store.service.CartService;
import com.development.store.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@Controller
@EnableJpaAuditing
public class CartController {
// Public:
    @ResponseBody @RequestMapping(value = "/api/addProduct", method = RequestMethod.POST)
    ResponseEntity<HashMap<String, String>> apiAddProduct(@RequestBody final CartDto dto, Model model) {
        HashMap<String, String> hashMap = new HashMap();
        String message = productService.checkProductQuantity(dto.getProductId(), dto.getQuantity());

        if (message != "") {
            hashMap.put("error_message", message);
            return ResponseEntity.ok(hashMap);
        }

        if (!cartService.addProduct(dto.getProductId(), dto.getQuantity())) {
            hashMap.put("error_message", "unknown error");
            return ResponseEntity.ok(hashMap);
        }

        Product product = productService.getById(dto.getProductId());

        if (product != null) {
            hashMap.put("productId", String.valueOf(product.getId()));
            hashMap.put("name", product.getName());
            hashMap.put("description", product.getDescription());
            hashMap.put("photo", product.getPhoto());
            hashMap.put("price", String.valueOf(product.getPrice()));
            hashMap.put("quantity", String.valueOf(cartService.getCartItemCount(dto.getProductId())));
            hashMap.put("manufacturer", product.getManufacturer());
            hashMap.put("category", product.getCategory());
            hashMap.put("collection", product.getCollection());
        }

        model.addAttribute("total", cartService.getTotal(cartService.getCartItems()));
        model.addAttribute("entry", cartService.getCartItems().size() > 9 ? "9+" : cartService.getCartItems().size());

        hashMap.put("totalPrice", String.valueOf(cartService.getTotal(cartService.getCartItems())));
        hashMap.put("productsQuantity", String.valueOf(cartService.getProductQuantity()));

        return ResponseEntity.ok(hashMap);
    }

    @ResponseBody @RequestMapping(value = "/api/removeProduct", method = RequestMethod.POST)
    ResponseEntity<HashMap<String, String>> apiRemoveProduct(@RequestBody final CartDto dto) {
        HashMap<String, String> hashMap = new HashMap();

        if (!cartService.removeProduct(dto.getProductId(), dto.getQuantity())) {
            hashMap.put("error_message", "unknown error");
            return ResponseEntity.ok(hashMap);
        }

        hashMap.put("productId", String.valueOf(dto.getProductId()));
        hashMap.put("quantity", String.valueOf(cartService.getCartItemCount(dto.getProductId())));
        hashMap.put("totalPrice", String.valueOf(cartService.getTotal(cartService.getCartItems())));
        hashMap.put("productsQuantity", String.valueOf(cartService.getProductQuantity()));

        return ResponseEntity.ok(hashMap);
    }

    @ResponseBody @RequestMapping(value = "/api/removeWholeProduct", method = RequestMethod.POST)
    ResponseEntity<HashMap<String, String>> apiRemoveWholeProduct(@RequestBody final CartDto dto) {
        HashMap<String, String> hashMap = new HashMap();
        if (!cartService.removeProduct(dto.getProductId(), cartService.getCartItemCount(dto.getProductId()))) {
            hashMap.put("error_message", "unknown error");
            return ResponseEntity.ok(hashMap);
        }

        hashMap.put("productId", String.valueOf(dto.getProductId()));
        hashMap.put("quantity", String.valueOf(cartService.getCartItemCount(dto.getProductId())));
        hashMap.put("totalPrice", String.valueOf(cartService.getTotal(cartService.getCartItems())));
        hashMap.put("productsQuantity", String.valueOf(cartService.getProductQuantity()));

        return ResponseEntity.ok(hashMap);
    }

// Private:
    @Autowired private ProductService productService;
    @Autowired private CartService cartService;
}
