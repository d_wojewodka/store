package com.development.store.controller;

import com.development.store.model.Address;
import com.development.store.model.User;
import com.development.store.service.AddressService;
import com.development.store.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;

@ControllerAdvice
@Controller
public class AddressController {
// Public:
    @PostMapping("/user/change/address") public String changeAddress(@RequestParam("name") String name,
                                                                     @RequestParam("last_name") String lastName,
                                                                     @RequestParam("address1") String address1,
                                                                     @RequestParam("address2") String address2,
                                                                     @RequestParam("city") String city,
                                                                     @RequestParam("postal_code") String postalCode,
                                                                     @RequestParam("country") String country,
                                                                     @RequestParam("phone") Long phone,
                                                                     HttpServletRequest request,
                                                                     Principal principal,
                                                                     HttpSession session) {
        User user = userService.getByUsername(principal.getName());
        Address address = userService.getAddress(user.getId());
        if (address == null) address = new Address();

        address.setUserId(user.getId());
        address.setName(name);
        address.setLastName(lastName);
        address.setAddress1(address1);
        address.setAddress2(address2);
        address.setCity(city);
        address.setPostalCode(postalCode);
        address.setCountry(country);
        address.setPhone(phone);

        addressService.save(address);

        return "redirect:/user/profile";
    }


// Private:
    @Autowired private AddressService addressService;
    @Autowired private UserService userService;
}
