package com.development.store.model;

import com.development.store.constants.GLOBAL;
import com.development.store.dto.AddressDto;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "address", schema = GLOBAL.SCHEMA_ACCOUNT, catalog = GLOBAL.SCHEMA_ACCOUNT)
public class Address {
// Public:
    public Address() {}
    public Address(final long id, final String name, final String lastName,
                   final String address1, final String address2, final String city,
                   final String postalCode, final String country, final Long phone) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.postalCode = postalCode;
        this.country = country;
        this.phone = phone;
    }

    public long getId() { return id; }
    public long getUserId() {
        return userId;
    }
    public String getName() { return name; }
    public String getLastName() { return lastName; }
    public String getAddress1() {
        return address1;
    }
    public String getAddress2() {
        return address2;
    }
    public String getCity() {
        return city;
    }
    public String getPostalCode() {
        return postalCode;
    }
    public String getCountry() {
        return country;
    }
    public Long getPhone() { return phone; }

    public void setId(final long id) { this.id = id; }
    public void setUserId(final long userId) {
        this.userId = userId;
    }
    public void setName(final String name) { this.name = name; }
    public void setLastName(final String lastName) { this.lastName = lastName;}
    public void setAddress1(final String address1) {
        this.address1 = address1;
    }
    public void setAddress2(final String address2) {
        this.address2 = address2;
    }
    public void setCity(final String city) {
        this.city = city;
    }
    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }
    public void setCountry(final String country) {
        this.country = country;
    }
    public void setPhone(final Long phone) {
        this.phone = phone;
    }

    public Address getFromDto(AddressDto addressDto) {
        name = Objects.nonNull(addressDto.getName()) ? addressDto.getName() : null;
        lastName = Objects.nonNull(addressDto.getLastName()) ? addressDto.getLastName() : null;
        address1 = Objects.nonNull(addressDto.getAddress1()) ? addressDto.getAddress1() : null;
        address2 = Objects.nonNull(addressDto.getAddress2()) ? addressDto.getAddress2() : null;
        city = Objects.nonNull(addressDto.getCity()) ? addressDto.getCity() : null;
        postalCode = Objects.nonNull(addressDto.getPostalCode()) ? addressDto.getPostalCode() : null;
        country = Objects.nonNull(addressDto.getCountry()) ? addressDto.getCountry() : null;
        phone = Objects.nonNull(addressDto.getPhone()) ? addressDto.getPhone() : null;

        return this;
    }

    @Override
    public String toString() {
        return "Address:" + "\n" +
                "\tid=" + id + "\n" +
                "\tuser_id=" + userId + "\n" +
                "\tname=" + name + "\n" +
                "\tlast_name=" + lastName + "\n" +
                "\taddress1=" + address1 + "\n" +
                "\taddress2=" + address2 + "\n" +
                "\tcity=" + city + "\n" +
                "\tpostal_code=" + postalCode + "\n" +
                "\tcountry=" + country + "\n" +
                "\tphone=" + phone;
    }

// Private:
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) private long id;
    @Column(name="user_id", nullable = false, unique = true) private long userId;
    @Column(name="name") private String name;
    @Column(name="last_name") private String lastName;
    @Column(name="address1") private String address1;
    @Column(name="address2") private String address2;
    @Column(name="city") private String city;
    @Column(name="postal_code") private String postalCode;
    @Column(name="country") private String country;
    @Column(name="phone") private Long phone;
}
