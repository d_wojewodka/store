package com.development.store.service.impl;

import com.development.store.model.Delivery;
import com.development.store.repository.DeliveryRepository;
import com.development.store.service.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Objects;

@Service
public class DeliveryServiceImpl implements DeliveryService {
// Public:
    final BigDecimal COURIER = new BigDecimal(10);
    final BigDecimal COURIER_EXPRESS = new BigDecimal(16);
    final BigDecimal BOX = new BigDecimal(8);
    final BigDecimal BOX_EXPRESS = new BigDecimal(12);
    final BigDecimal POST = new BigDecimal(4);
    final BigDecimal POST_EXPRESS = new BigDecimal(7);

    public DeliveryServiceImpl () {}
    public DeliveryServiceImpl (final DeliveryRepository deliveryRepository) {
        this.deliveryRepository = deliveryRepository;
    }

    @Override
    public Delivery save(final Delivery delivery) {
        try {
            Delivery newDelivery = deliveryRepository.save(delivery);
            return newDelivery;
        }
        catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean delete(final Delivery delivery) {
        try {
            deliveryRepository.delete(delivery);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public BigDecimal calculateDeliveryCost(final int deliveryId) {
        BigDecimal cost;

        switch (deliveryId) {
            case 1: { cost = this.COURIER; break; }
            case 2: { cost = this.COURIER_EXPRESS; break; }
            case 3: { cost = this.BOX; break; }
            case 4: { cost = this.BOX_EXPRESS; break; }
            case 5: { cost = this.POST; break; }
            case 6: { cost = this.POST_EXPRESS; break; }

            default: {
                System.err.println("Uknown delivery type, propably some exploit");
                return new BigDecimal(0);
            }

        }

        return cost;
    }

    @Override
    public Collection<Delivery> getAvailable() {
        Collection<Delivery> delivery = deliveryRepository.getAvailableDeliveries();
        return Objects.nonNull(delivery) ? delivery : null;
    }

    @Override
    public HashMap<String, BigDecimal> getAvailableDeliveries() {
        HashMap<String, BigDecimal> deliveries = new HashMap<>();
        Collection<Delivery> delivery = deliveryRepository.getAvailableDeliveries();

        for (Delivery type : delivery) {
            deliveries.put(type.getName(), type.getPrice());
        }

        return deliveries;
    }

// Private:
    @Autowired private DeliveryRepository deliveryRepository;
}
