package com.development.store.service;

import com.development.store.model.Role;

public interface RoleService {
    Role save(Role role);
    boolean delete(Role role);

    Role getById(long id);
    Role getByName(String name);

}
