package com.development.store.model;

import com.development.store.constants.GLOBAL;
import com.development.store.dto.ProductDto;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "product", schema = GLOBAL.SCHEMA_DATA, catalog = GLOBAL.SCHEMA_DATA)
public class Product {
// Public:
    public Product() {};
    public Product(final String name, final String description, final String photo,
                   final String photoList, final BigDecimal price, final long quantity,
                   final String manufacturer, final String category, final String collection) {
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.photoList = photoList;
        this.price = price;
        this.quantity = quantity;
        this.manufacturer = manufacturer;
        this.category = category;
        this.collection = collection;
    }

    public long getId() { return id; }
    public String getName() { return name; }
    public String getDescription() { return description; }
    public String getPhoto() { return photo; }
    public String getPhotoList() { return photoList; }
    public BigDecimal getPrice() { return price; }
    public long getQuantity() { return quantity; }
    public String getManufacturer() { return manufacturer; }
    public String getCategory() { return category; }
    public String getCollection() { return collection; }

    public void setId(final long id) { this.id = id; }
    public void setName(final String name) { this.name = name; }
    public void setDescription(final String description) { this.description = description; }
    public void setPhoto(final String photo) { this.photo = photo; }
    public void setPhotoList(final String photoList) { this.photoList = photoList; }
    public void setPrice(final BigDecimal price) { this.price = price; }
    public void setQuantity(final long quantity) { this.quantity = quantity; }
    public void setManufacturer(final String manufacturer) { this.manufacturer = manufacturer; }
    public void setCategory(final String category) { this.category = category; }
    public void setCollection(final String collection) { this.collection = collection; }

    public Product getFromDto(ProductDto productDto) {
        id = Objects.nonNull(productDto.getProductId()) ? productDto.getProductId() : null;
        name = Objects.nonNull(productDto.getName()) ? productDto.getName() : null;
        description = Objects.nonNull(productDto.getDescription()) ? productDto.getDescription() : null;
        photo = Objects.nonNull(productDto.getPhoto()) ? productDto.getPhoto() : null;
        photoList = Objects.nonNull(productDto.getPhotoList()) ? productDto.getPhotoList() : null;
        price = Objects.nonNull(productDto.getPrice()) ? productDto.getPrice() : null;
        quantity = Objects.nonNull(productDto.getQuantity()) ? productDto.getQuantity() : null;
        manufacturer = Objects.nonNull(productDto.getManufacturer()) ? productDto.getManufacturer() : null;
        category = Objects.nonNull(productDto.getCategory()) ? productDto.getCategory() : null;
        collection = Objects.nonNull(productDto.getCollection()) ? productDto.getCollection() : null;

        return this;
    }

    @Override
    public String toString() {
        return "Product:" + "\n" +
                "\tid=" + id + "\n" +
                "\tname=" + name + "\n" +
                "\tdescription=" + description + "\n" +
                "\tphoto=" + photo + "\n" +
                "\tphoto_list=" + photoList + "\n" +
                "\tprice=" + price + "\n" +
                "\tquantity=" + quantity + "\n" +
                "\tmanufacturer=" + manufacturer + "\n" +
                "\tcategory=" + category + "\n" +
                "\tcollection=" + collection;
    }

// Private:
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) private long id;
    @Column(name="name", nullable = false, unique = true) private String name;
    @Column(name="description", nullable = false) private String description;
    @Column(name="photo") private String photo;
    @Column(name="photo_list") private String photoList;
    @Column(name="price", nullable = false) private BigDecimal price;
    @Column(name="quantity", nullable = false) private long quantity;
    @Column(name="manufacturer", nullable = false) private String manufacturer;
    @Column(name="category", nullable = false) private String category;
    @Column(name="collection", nullable = false) private String collection;
}
