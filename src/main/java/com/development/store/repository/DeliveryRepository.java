package com.development.store.repository;

import com.development.store.model.Delivery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

public interface DeliveryRepository extends JpaRepository<Delivery, Long> {
    @Query("SELECT r FROM Delivery r WHERE r.available = 1")
    Collection<Delivery> getAvailableDeliveries();

    @Query("SELECT r FROM Delivery r")
    Collection<Delivery> getAllDeliveries();
}
