package com.development.store;

import com.development.store.model.Cart;
import com.development.store.repository.CartRepository;
import com.development.store.service.CartService;
import com.development.store.service.impl.CartServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CartTests {
// Public:
    @Test @Order(1)
    public void save() {
        // given
        Cart cart = new Cart(1, 1, 1);

        // then
        Mockito.lenient().when(cartRepository.save(Mockito.any(Cart.class))).thenReturn(cart);
        Cart newCart = cartService.save(cart);

        Assertions.assertNotNull(newCart);
        Assertions.assertEquals(1, newCart.getUserId());
        Assertions.assertEquals(1, newCart.getProductId());
        Assertions.assertEquals(1, newCart.getQuantity());

        // given
        Collection<Cart> carts = new ArrayList();
        for (int i = 1; i < 10; i++) {
            Cart entry = new Cart(1, i, 3+(3*i*i));
            carts.add(entry);
        }

        // then
        Mockito.lenient().when(cartRepository.saveAll(Mockito.any(Collection.class))).thenReturn((List) carts);
        Collection<Cart> newCarts = cartService.saveAll(carts);

        Assertions.assertNotNull(newCarts);

        for (Cart entry : newCarts) {
            Assertions.assertNotNull(entry);
            int i = (int) entry.getProductId();
            Assertions.assertEquals(3+(3*i*i), entry.getQuantity());
        }
    }

    @Test @Order(2)
    public void get() {
        // given
        Cart cart = new Cart(1, 1, 1);

        // then
        Mockito.lenient().when(cartRepository.getByProduct(Mockito.any(long.class), Mockito.any(long.class))).thenReturn(cart);
        Cart newCart = cartService.getByProduct((long) 1, (long) 1);

        Assertions.assertNotNull(newCart);

        // given
        Collection<Cart> carts = new ArrayList();
        for (int i = 1; i < 10; i++) {
            Cart entry = new Cart(1, i, 3+(3*i*i));
            carts.add(entry);
        }

        // then
        Mockito.lenient().when(cartRepository.getByUser(Mockito.any(long.class))).thenReturn(carts);
        Collection<Cart> newCarts = cartService.getByUser(1);

        Assertions.assertNotNull(newCarts);
        for (Cart entry : newCarts) {
            int i = (int) entry.getProductId();
            Assertions.assertEquals(3+(3*i*i), entry.getQuantity());
        }
    }

    @Test @Order(3)
    public void modifyCart() {
        // given
        Cart cart = new Cart(1, 1, 1);

        // then
        Mockito.lenient().when(cartRepository.save(Mockito.any(Cart.class))).thenReturn(cart);
        Mockito.lenient().when(cartRepository.getByProduct(Mockito.any(long.class), Mockito.any(long.class))).thenReturn(cart);
        Cart result = cartService.changeQuantity(1, 1, 10);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(11, result.getQuantity());

        result = cartService.changeQuantity(1, 1, -10);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(1, result.getQuantity());

//        // given
//        Collection<Cart> carts = new ArrayList();
//        for (int i = 1; i < 10; i++) {
//            Cart entry = new Cart(1, i, 3+(i*2));
//            entry.setId(i);
//            carts.add(entry);
//        }
//
//        // then
//        Mockito.lenient().when(cartRepository.saveAll(Mockito.any(Collection.class))).thenReturn((List) carts);
//        Collection<Cart> results = cartService.saveAll(carts);
//
//        Assertions.assertNotNull(results);
//        for (Cart entry : results) {
//            System.err.println("entry -> id: " + entry.getId() + ", userId: " + entry.getUserId() + ", productId: " + entry.getProductId() + ", quantity: " + entry.getQuantity());
//            cart = cartService.save(entry);
//            Cart newCart = cartService.changeQuantity(1, cart.getProductId(), (int) cart.getProductId()* (int) cart.getProductId());
//            Assertions.assertEquals((3+(newCart.getProductId()*2) + 1 + newCart.getProductId()*newCart.getProductId()), newCart.getQuantity());
//        }
    }

// Private:
    @InjectMocks
    private CartService cartService = new CartServiceImpl();

    @Mock
    private CartRepository cartRepository;
}
