package com.development.store.dto;

import com.development.store.model.Product;

import java.math.BigDecimal;
import java.util.Objects;

public class ProductDto {
    // Public:
    public ProductDto() {};
    public ProductDto(final long productId, final String name, final String description,
                      final String photo, final String photoList, final BigDecimal price,
                      final String manufacturer, final String category, final String collection) {
        this.productId = productId;
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.photoList = photoList;
        this.price = price;
        this.manufacturer = manufacturer;
        this.category = category;
        this.collection = collection;
    }

    public long getProductId() { return productId; }
    public String getName() { return name; }
    public String getDescription() { return description; }
    public String getPhoto() { return photo; }
    public String getPhotoList() { return photoList; }
    public BigDecimal getPrice() { return price; }
    public long getQuantity() { return quantity; }
    public String getManufacturer() { return manufacturer; }
    public String getCategory() { return category; }
    public String getCollection() { return collection; }

    public void setProductId(final long productId) { this.productId = productId; }
    public void setName(final String name) { this.name = name; }
    public void setDescription(final String description) { this.description = description; }
    public void setPhoto(final String photo) { this.photo = photo; }
    public void setPhotoList(final String photoList) { this.photoList = photoList; }
    public void setPrice(final BigDecimal price) { this.price = price; }
    public void setQuantity(long quantity) { this.quantity = quantity; }
    public void setManufacturer(final String manufacturer) { this.manufacturer = manufacturer; }
    public void setCategory(final String category) { this.category = category; }
    public void setCollection(final String collection) { this.collection = collection; }

    public ProductDto getFromEntity(Product product) {
        productId = Objects.nonNull(product.getId()) ? product.getId() : null;
        name = Objects.nonNull(product.getName()) ? product.getName() : null;
        description = Objects.nonNull(product.getDescription()) ? product.getDescription() : null;
        photo = Objects.nonNull(product.getPhoto()) ? product.getPhoto() : null;
        photoList = Objects.nonNull(product.getPhotoList()) ? product.getPhotoList() : null;
        price = Objects.nonNull(product.getPrice()) ? product.getPrice() : null;
        quantity = Objects.nonNull(product.getQuantity()) ? product.getQuantity() : null;
        manufacturer = Objects.nonNull(product.getManufacturer()) ? product.getManufacturer() : null;
        category = Objects.nonNull(product.getCategory()) ? product.getCategory() : null;
        collection = Objects.nonNull(product.getCollection()) ? product.getCollection() : null;

        return this;
    }

//// Static:
//    public static Product DtoToEntity(ProductDto dto) {
//        Product product = new Product();
//
//        product.setId(dto.getProduct().getId());
//        private String name;
//        private String description;
//        private String images;
//        private BigDecimal price;
//        private long quantity;
//        private String manufacturer;
//        private String category;
//        private String collection;
//
//        return product;
//    }
//
//    public static ProductDto EntityToDto(Product product) {
//        ProductDto dto = new ProductDto();
//
//        //dto.setId(address.getId());
//        //dto.setUserId(address.getUserId());
//        dto.setName(address.getName());
//        dto.setLastName(address.getLastName());
//        dto.setAddress1(address.getAddress1());
//        dto.setAddress2(address.getAddress2());
//        dto.setCity(address.getCity());
//        dto.setPostalCode(address.getPostalCode());
//        dto.setCountry(address.getCountry());
//        dto.setPhone(address.getPhone());
//
//        return dto;
//    }

    // Private:
    private long productId;
    private String name;
    private String description;
    private String photo;
    private String photoList;
    private BigDecimal price;
    private long quantity;
    private String manufacturer;
    private String category;
    private String collection;
}
