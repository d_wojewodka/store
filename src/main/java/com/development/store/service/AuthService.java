package com.development.store.service;

public interface AuthService {
    boolean isLoggedIn();
    String getName();
}
