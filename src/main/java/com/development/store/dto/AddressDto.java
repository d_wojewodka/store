package com.development.store.dto;

import com.development.store.model.Address;

import java.util.Objects;

public class AddressDto {
// Public:
    public AddressDto() {}
    //public AddressDto(final long addressId, final String name, final String lastName,
    public AddressDto(final String name, final String lastName,
                      final String address1, final String address2, final String city,
                      final String postalCode, final String country, final Long phone) {
        //this.addressId = addressId;
        this.name = name;
        this.lastName = lastName;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.postalCode = postalCode;
        this.country = country;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Address:" + "\n" +
                //"\taddressId=" + addressId + "\n" +
                "\tname=" + name + "\n" +
                "\tlast_name=" + lastName + "\n" +
                "\taddress1=" + address1 + "\n" +
                "\taddress2=" + address2 + "\n" +
                "\tcity=" + city + "\n" +
                "\tpostal_code=" + postalCode + "\n" +
                "\tcountry=" + country + "\n" +
                "\tphone=" + phone;
    }

    //public long getAddressId() { return addressId; }
    public String getName() { return name; }
    public String getLastName() { return lastName; }
    public String getAddress1() { return address1; }
    public String getAddress2() { return address2; }
    public String getCity() { return city; }
    public String getPostalCode() { return postalCode; }
    public String getCountry() { return country; }
    public long getPhone() { return phone; }

    //public void setAddressId(final long addressId) {
    //    this.addressId = addressId;
    //}
    public void setName(final String name) {
        this.name = name;
    }
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }
    public void setAddress1(final String address1) {
        this.address1 = address1;
    }
    public void setAddress2(final String address2) {
        this.address2 = address2;
    }
    public void setCity(final String city) {
        this.city = city;
    }
    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }
    public void setCountry(final String country) {
        this.country = country;
    }
    public void setPhone(final Long phone) {
        this.phone = phone;
    }

    public AddressDto getFromEntity(Address address) {
        name = Objects.nonNull(address.getName()) ? address.getName() : null;
        lastName = Objects.nonNull(address.getLastName()) ? address.getLastName() : null;
        address1 = Objects.nonNull(address.getAddress1()) ? address.getAddress1() : null;
        address2 = Objects.nonNull(address.getAddress2()) ? address.getAddress2() : null;
        city = Objects.nonNull(address.getCity()) ? address.getCity() : null;
        postalCode = Objects.nonNull(address.getPostalCode()) ? address.getPostalCode() : null;
        country = Objects.nonNull(address.getCountry()) ? address.getCountry() : null;
        phone = Objects.nonNull(address.getPhone()) ? address.getPhone() : null;

        return this;
    }

// Private:
    private String name;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String postalCode;
    private String country;
    private Long phone;
}
