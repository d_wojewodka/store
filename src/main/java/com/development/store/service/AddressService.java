package com.development.store.service;

import com.development.store.model.Address;

import java.util.Collection;

public interface AddressService {
    Address save(Address addres);
    boolean delete(Address addres);

    Collection<Address> getAll();

    Address getById(long id);
    Address getByUserId(long id);
}
