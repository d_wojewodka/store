package com.development.store.service.impl;

import com.development.store.model.Address;
import com.development.store.model.Order;
import com.development.store.repository.OrderRepository;
import com.development.store.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {
// Public:
    public OrderServiceImpl() {}
    public OrderServiceImpl(final OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Order save(Order order) {
        return order;
    }

    @Override
    public boolean delete(Order order) {
        return false;
    }

    @Override
    public boolean initializeOrder(long userId, Address address) {
        return false;
    }

// Private:
    @Autowired private OrderRepository orderRepository;
}
